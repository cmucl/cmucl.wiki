<!-- Name: InstallingCmucl -->
<!-- Version: 6 -->
<!-- Last-Modified: 2014/10/12 09:27:56 -->
<!-- Author: rtoy -->
[[_TOC_]]

# Prerequisites
Cmucl is still a 32-bit application so to be able to run on the most
recent OSes, you need to have the 32-bit libraries and other tools
installed.  See [build tools](Developer/BuildTools) for information on
the libraries needed for various OSes.  The C compiler is not
necessary unless you want to dump lisp executables.  In that case, the
compiler is needed.

# Installing CMUCL
This page explains how to install the release tarballs of CMUCL. If
you're using an alternative distribution packaging, such as .deb
archives, use whatever is appropriate there. We use the 19e release as
an example, but the same idea can be used for any release tarball or
snapshot for CMUCL.

For each [supported platform](http://cmucl.org/platforms.html) there
are two bzip2 tar archives, one containing the base system, and the
other (with extra in the name) containing optional additional files
supporting CLX, Hemlock, the Motif interface, and so on. Note also
that there may also be Unicode and non-Unicode (8-bit characters)
tarballs. The non-Unicode tarball has `non-unicode` in the file
name. The same method can be used to install either of these.

Download the archives corresponding to your platform. Certain tarballs
are PGP-signed by the packager and are accompanied by a file with an
.asc extension; in this case you can check that they have not been
modified by using GPG:

```
wget http://common-lisp.net/project/cmucl/downloads/release/19e/cmucl-19e-x86-linux.tar.bz2
wget http://common-lisp.net/project/cmucl/downloads/release/19e/cmucl-19e-x86-linux.tar.bz2.asc
gpg --verify cmucl-19e-x86-linux.tar.bz2.asc
```

GPG should tell you that the file has a good signature from one of the
CMUCL developers (you may need to fetch the developer's key with 
`gpg --recv-key`). Unless you have a web-of-trust relationship with that
developer it will warn you that the key is not certified with a
trusted signature.  

## Extract the tarballs

The release tarballs extract to the following directory structure:
```
bin/lisp
lib/cmucl/lib/lisp.core
doc/cmucl/README (this file)
man/man1/cmucl.1
 ...
```

This allows you to install CMUCL directly under /usr/local, for example using
```
cd /usr/local
tar xjf /path/to/cmucl-19e-<platform>.tar.bz2
tar xjf /path/to/cmucl-19e-<platform>.extra.tar.bz2
```
If you do not have a version of `tar` that understand the `j`
option, you need to replace the `tar` command above with
```
bunzip2 < /path/to/cmucl-19e-<platform>.tar.bz2 | tar xf -
```

Alternatively, you may install under a directory in /opt (or any other
directory where you have write privileges). For example
```
mkdir /opt/cmucl-19e
cd /opt/cmucl-19e
tar xjf /path/to/cmucl-19e-<platform>.tar.bz2
tar xjf /path/to/cmucl-19e-<platform>.extra.tar.bz2
```

The 19e distribution is relocateable: the lisp binary will search for
the lisp.core file relative to its location. This means that it is
sufficient to have /usr/local/bin (or /opt/cmucl-19e/bin) in your PATH
to be able to invoke CMUCL from your shell.

You can now invoke CMUCL: this should display a banner then show a
prompt (the default prompt is an asterisk).
```
% lisp
CMU Common Lisp 19e (19E), running on mansuetude
With core: /opt/cmucl-19e/lib/cmucl/lib/lisp.core
Dumped on: Thu, 2008-05-01 18:56:07+02:00 on usrtc3142
See <http://www.cons.org/cmucl/> for support information.
Loaded subsystems:
    Python 1.1, target Intel x86
    CLOS based on Gerd's PCL 2004/04/14 03:32:47
* (format t "~&Hello, world!~%")
Hello, world!
NIL
*
```

## Loading subsystems

To load precompiled subsystems (assuming that you installed the
-extra- tarball), just use `REQUIRE`:
```
* (require :gray-streams)
* (require :clx)
* (require :clm)
* (require :hemlock)
```

Starting with the 2010-06 snapshot, ASDF2 and MK-DEFSYSTEM are
included with CMUCL. There you can load them using
```
* (require :asdf)      ; Load asdf2
* (require :defsystem) : Load mk-defsystem

```

### Known Issues
#### Hemlock
Hemlock [needs Courier fonts](http://trac.common-lisp.net/cmucl/ticket/85).  If you get an error starting hemlock,
make sure you have Courier fonts installed.  You can check using
```
xlsfonts | grep 'adobe-courier'
```

#### CLM
CLM uses `motifd`.  Currently, this is built as a 64-bit binary.
This, of course, will [not work](http://trac.common-lisp.net/cmucl/ticket/84) if you are running on a 32-bit OS.  In
this case, you may want to replace your motifd with this 
[32-bit motifd](http://common-lisp.net/~rtoy/cmucl/motifd).


## Create a site-wide initialization file

CMUCL reads two initialization files: a per-user file named
`~/.cmucl-init.lisp`, and a site-wide file named
`$ROOT/lib/cmucl/lib/site-init.lisp` (where `$ROOT` is the
base directory in which you installed CMUCL, for example
`/usr/local/`). It is customary to initialize the
`SHORT-SITE-NAME` and `LONG-SITE-NAME` in the site-init
file.

CMUCL is distributed with a file called
`$ROOT/lib/cmucl/lib/generic-site.lisp` that you can use as a template
for your site-init file:
```
   # cd $ROOT/lib/cmucl/lib
   # cp generic-site.lisp site-init.lisp
   # emacs site-init.lisp
```

If you have the CMUCL source code installed on your system, you can
set the target: search-list to point to them, so that the debugger
provides more information when a CMUCL function signals an error. See
the commented-out line at the end of the generic-site file.

## Interacting with CMUCL

The basic commandline interface offered by CMUCL is rather basic; it
doesn't offer a history mechanism or commandline editing
facilities. You may wish to investigate using a more powerful
interface than the commandline, such as
[SLIME](http://common-lisp.net/project/slime/), or
[Climacs](http://common-lisp.net/project/climacs/), or
[Hemlock](http://cmucl.org/hemlock/),
or (more basic) M-x run-lisp in Emacs.  

## Patching CMUCL

On occasion, patches for CMUCL releases are created for critical
issues found after the release. These are normally fixed by the next
snapshot, but in case it is not desirable to use the snapshot, patches
are created. The patches are in the patches subdirectory of the
release directory. These patches are tarballs containing an asd file
and the necessary files to implement the patch. To apply a patch, you
need to have [asdf](http://www.cliki.net/asdf) available. (Note that 20b now includes asdf2.) Untar
the patch somewhere where asdf will be able to find it. Then simply
use
```
(require :cmucl-xxx-patch-nnn)
```

where `xx` is the the release version of cmucl (such as 19a) and `nnn` is
the patch number. This works if your version of asdf has the hooks
needed for this. Otherwise, you can do
```
(load "patch-nnn.asd")
(asdf:oos 'asdf:load-op :cmucl-xx-patch-nnn)
```
