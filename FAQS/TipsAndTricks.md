<!-- Name: TipsAndTricks -->
<!-- Version: 2 -->
<!-- Last-Modified: 2014/09/04 20:52:28 -->
<!-- Author: rtoy -->
# Tips and Tricks

Here are some tips and tricks on using CMUCL

 * [ChangeCmuclPrompt](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/ChangeCmuclPrompt)
 * [CustomCommandLineSwitches](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/CustomCommandLineSwitches)
 * [AnalyzingMemoryUsage](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/AnalyzingMemoryUsage)
 * [TuningGc](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/TuningGc)
 * [ReadingDisassembly](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/ReadingDisassembly)
 * [FeatureList](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/FeatureList)
