Please read these before asking questions on the cmucl-help and cmucl-imp mailing lists. Additional questions and answers can be sent to the webmasters.

1. How do I turn off these GC messages?

   ```
   [GC threshold exceeded with 10,411,328 bytes in use.  Commencing GC.]
   [GC completed with 990,320 bytes retained and 9,421,008 bytes freed.]
   [GC will next occur when at least 8,990,320 bytes are in use.]
   ```

   **Answer**: Add `(setq ext:*gc-verbose* nil)` to your `~
   /.cmucl-init` initialization file. See the CMUCL User's Manual for
   more information on tuning the garbage collector.

2. Why does CMUCL say `Warning: Declaring foo special`?

   **Answer**: This happens when you have used SETQ on an undeclared variable at the top level. The default behaviour of CMUCL in this situation is to [declare the variable special](http://www.lispworks.com/documentation/HyperSpec/Body/d_specia.htm) (transforming it from a lexically bound variable to a dynamically bound variable). In effect, when you do

   ```
      (setq foo 42)
   ```

   and `foo` has not previously been declared (using `DEFVAR` or `DEFPARAMETER` for example), CMUCL will implicitly do a

   ```
      (defvar foo)
   ```

   for you. This is done for the user's convenience, but can lead to strange behaviour if you're not expecting it (since by convention special variables are named with surrounding *asterisks*), which is why CMUCL emits the warning message. Also note that there is no way of undoing the SPECIAL declaration, to allow purely lexical binding of the symbol.

   The variable `ext:*top-level-auto-declare*` allows you to control this behaviour.

1. How do I compile my Lisp program to an executable?

   **Answer**: CMUCL does not, in general, support delivery as an executable. If this bothers you, note that this is also the case of most other programming language implementations: for example Sun's java implementation requires a bundle of class files.

   The standard way of delivering a Common Lisp application with CMUCL is to dump an image containing all your application code (see the CMUCL User's Manual for details), and deliver a tarball containing this image, the lisp runtime, and a shell script which launches the runtime with your image (see the sample-wrapper distributed with CMUCL for guidance). Also see the following [hint on making Lisp files executable](https://cmucl.org/doc/executable.html).

   However, on Linux and FreeBSD x86 platforms, CMUCL can actually
   produce an executable. This is done by specifying :executable t
   option for save-lisp. The executable file contains the current
   entire core image and runtime. See the section Saving a Core Image
   in the [CMUCL User's Manual](https://cmucl.org/docs/cmu-user/html/index.html) for more information.

1. Why does CMUCL say it's called Python 1.1 when it starts up? Isn't that the name of a scripting language?

   **Answer**: The CMUCL native code compiler is called Python. This use of the name predates the existence of that other scripting language.

   On the history of the name, Rob MacLachlan says

   > Scott Fahlman said that he wanted a really smart compiler that
   > would digest your program the way a Python digests a pig. It was a
   > colorful metaphor for the idea of a compiler that pushed farther in
   > the direction of trading compile efficiency for runtime
   > efficiency. We achieved this to such a degree that the original
   > [Spice](https://en.wikipedia.org/wiki/Spice_Lisp) machine, the [Perq](https://en.wikipedia.org/wiki/PERQ), couldn't usefully run Python, though we
   > tried it. It ran first on the IBM RT PC. This was cross-compiled
   > using the previous Spice Lisp compiler (CLC, Common Lisp Compiler),
   > which had a large runtime of assembler written by David
   > B. Mcdonald, and the first Python backend used all of that assembly
   > runtime. The first port setting the pattern of later ports was the
   > MIPS port, which was largely done by William Lott.

   Scott Fahlman says he

   > thought Python was a good name because a compiler is a long pipeline. A pig goes in one end, the snake goes off to rest under a bush for a surprisingly long time, the pipeline does its thing, and compact little pellet eventually comes out the other end.

1. How do I report a bug in CMUCL?

   **Answer**: Send an email describing the problem to the cmucl-help xor cmucl-imp mailing lists (see the [Support page](https://cmucl.common-lisp.dev/support.html) for more information on these lists). Make sure you include the version of CMUCL that you are using (for instance the herald that it prints on startup), the platform, your `*features*`. Make sure that the problem isn't coming from your personal or site-wide initialization files. Try to find the smallest input file that provokes the problem.

1. How does CMUCL compare with other Common Lisp implementations?

   **Answer**: The short answer is that this really depends on your needs. Most free implementations are fairly easy to install, and you should be able to obtain evaluation copies of the commercial implementations, so it isn't difficult to make a choice yourself.

   A longer answer is that compared with the various commercial Common Lisp implementations (see [lisp.org](https://www.lisp.org/) for a list), CMUCL is free: you can use it for zero upfront cost, and it has a very liberal license that imposes few restrictions on its use and redistribution. You also have access to the source code if you wish to customize the implementation to your requirements. However, you can't get a support contract from the vendor (though you could probably find experienced CMUCL developers prepared to freelance -- contact the cmucl-imp mailing list), and CMUCL is missing certain features present in the commercial implementations (portability to Microsoft Windows platforms, graphical browsers, GUI widgets, add-on libraries ...).

   Compared with [CLISP](https://clisp.cons.org/), CMUCL runs on fewer platforms and has significantly higher memory usage. CLISP has better internationalization support, in particular support for UNICODE. Since CMUCL compiles to native code, it is an order of magnitude faster on most applications than CLISP's bytecode execution model, and generally provides more useful debugging output. However, the mathematical primitives in CLISP are very fast, in particular its bignum operations. CLISP also provides floats of unlimited precision, while CMUCL is limited to IEEE-754 single-float and double-float, and an extended double-double-float. CMUCL has a more powerful foreign function interface than CLISP, and supports multiprocessing on x86 platforms.

   Compared with [SBCL](https://sbcl.sf.net/) (a fork from the CMUCL implementation), CMUCL a different set of features (it includes a Motif interface, but does not have SBCL's native threads on Linux/x86 platforms, nor Unicode support). CMUCL has a faster compiler, but compiled code runs at a similar speed to SBCL-generated code. SBCL is closer to the ANSI CL specification in some respects, and generally emits more warnings about ANSI-compliance. SBCL runs on a larger number of platforms than CMUCL, and in general is more actively developed than CMUCL.

1. What user interface do you use with CMUCL?

   **Answer**: Many people like to use [SLIME](https://common-lisp.net/project/slime/) in Emacs.

1. How difficult is it to port CMUCL to a new platform?

   **Answer**: Short answer: fairly difficult. There are two aspects to porting: writing a backend for the new the CPU architecture, and handling the runtime's interaction with the operating system.

   Writing a compiler backend to target a new CPU involves deciding on a register allocation policy, and writing assembly definitions for the CMUCL virtual machine operations (VOPs). There are also a number of utility routines to write in assembly, and some CPU-specific constants (number of registers, how FPU exceptions are reported to user land, how wide floating point registers are) to define.

   Targeting a new operating system involves getting the runtime to compile. This means stuff like deciding on a memory map, implementing memory management routines. The trickiest bit is probably signal handling, and extracting the address of the faulting instruction from a ucontext_t.

1. The garbage collector doesn't want to collect my big object!

   **Answer**: You may have done something like

   ```
   USER> (setq *print-array* nil)
   USER> (defvar *big* (make-array 42000000 :element-type 'double-float))
   ;; use big, then get rid of it
   USER> (setq *big* nil)
   USER> (gc :full t)    ;; :full only with generational collector
   ```

   You no longer have any references to the array, so were expecting it to be garbage collected. However, according to `(ROOM)` it wasn't. The reason is that the [read-eval-print-loop](http://www.lispworks.com/documentation/HyperSpec/Body/25_aa.htm) maintains variables called `*`, `**` and `***`, that reference the values of the last three forms evaluated, and the array is still accessible from these. Try evaluating a few other forms (like 1), then call the garbage collector again.

   (This question isn't specific to CMUCL; you'll observe the same in other implementations.)

1. CMUCL dies with `*A2 gc_alloc_large failed, nbytes=NNN`

   **Answer**: This is the generational conservative garbage collector telling you that you have exhausted the available dynamic space (the memory zone that is used for normal lisp objects). You can increase the size of the dynamic space reserved on startup by using the `-dynamic-space-size` commandline option (see the [CMUCL User's Manual](https://cmucl.org/docs/cmu-user/html/index.html) for details). You can determine the amount of dynamic space currently available as follows:

   ```
   USER> (alien:def-alien-variable ("dynamic_space_size" dynamic-space-size) c-call::int)
   #<ALIEN::HEAP-ALIEN-INFO
     (SYSTEM:FOREIGN-SYMBOL-ADDRESS '"dynamic_space_size") (ALIEN:SIGNED 32)>
   USER> dynamic-space-size
   536870912
   ```
1. What does `Error in function UNIX::SIGSEGV-HANDLER: Segmentation
   Violation at #x1004C7BD`. mean?

   **Answer**: This means that CMUCL has received a signal indicating a segmentation violation from the operating system, at an unexpected address (it already uses SIGSEGV for normal operation of the garbage collector). This can be due to:

   - you have linked with some alien code (such as a shared library) which is generating segmentation violations. This can be due to a bug in the alien code, or to you passing it invalid pointers.
   - you have lied to the compiler (written incorrect type declarations), and compiled your code in unsafe mode (with the speed optimization quality higher than the safety quality). For example, you may have declared that a variable was an array, and actually passed a list to the function. Make sure that you compile and run your code in safe mode before trying to increase its speed.
   - you may have encountered an internal bug in CMUCL. It's quite unlikely that a bug should manifest itself in this way, though, so please check the first two possibilities before reporting a bug.

1. Where can I hang out with CMUCL folks on IRC?

   **Answer**: Try the `#lisp` channel on the [freenode network](https://freenode.net/). A number of CMUCL users and developers (as well as SBCL creatures) can occasionally be found wasting time there.

1. CMUCL leaks too much stuff from the compile-time environment to the evaluation environment!

   **Answer**: You may encounter this problem when porting code written for CMUCL to another Common Lisp implementation -- such as LispWorks or OpenMCL -- which is more conservative than CMUCL in propagating declarations to the evaluation environment. For instance, consider the compilation of a file containing the following code:
   ```
   (defconstant +foo+ #\a)
   (defun foo ()
     #.(char-code +foo+))
   ```
   This code will compile in CMUCL, but some other implementations will complain that the symbol `+foo+` is not bound when compiling the function `foo`. CMUCL propagates the compile-time effect of the DEFCONSTANT form to what CLtS calls the *evaluation environment*, so that it becomes available when compiling the remainder of the file. Certain other implementations are stricter in not leaking information in this way, and require you to write the above code as
   ```
   (eval-when (:compile-toplevel :load-toplevel)
     (defconstant +foo+ #\a))
   (defun foo ()
     #.(char-code +foo+))
   ```
   This code will also work in CMUCL. See [Section 3.2.3.1 Processing of Top Level Forms](http://www.lispworks.com/documentation/HyperSpec/Body/03_bca.htm) of CLtS for more details (the specification is somewhat vague about some of these issues, which explains why there is considerable variation in behaviour from one implementation to another).

1. What is the meaning of the error message I see when tracing functions: :FUNCTION-END breakpoints are currently unsupported for the known return convention ?

   **Answer**: This is a deficiency in the tracing support that has been fixed. It shouldn't happen anymore, but if you do see this, please report this bug. A simple workaround is to use
   ```
   USER> (trace my-function :encapsulate t)
   ```
   This causes a different mechanism to be used for the breakpoints that are used by tracing facility.

1. On my Linux machine, CMUCL dies on startup saying
   ```
   Error in allocating memory, please do "echo 1 > /proc/sys/vm/overcommit_memory"
   or get more memory+swap.
   ```
   **Answer**: Hopefully the message is fairly clear. The problem is
   that due to implementation choices, CMUCL reserves a large address
   space when it starts up, instead of allocating memory on demand, as
   do most applications. In its default configuration, the linux
   kernel may refuse to reserve amounts of memory which are far
   greater than the amout of available RAM and swap (this is called
   overcommitting, since the kernel commits itself to satisfy more
   memory than is actually available). You can either increase the
   amount of swap available (see the mkswap command), or change the
   kernel's policy using (as root) the command quoted above.


