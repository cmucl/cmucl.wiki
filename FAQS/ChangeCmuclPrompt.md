<!-- Name: ChangeCmuclPrompt -->
<!-- Version: 1 -->
<!-- Last-Modified: 2012/01/08 14:51:41 -->
<!-- Author: rtoy -->
# Changing the default CMUCL Prompt

The default CMUCL prompt is a fairly basic `*`. You can use the
`*PROMPT*` variable to customize this prompt, either by setting it to a
constant string, or by setting it to a function which will be called
each time the prompt is displayed.

The following code demonstrates how to make CMUCL print the current
package in the prompt.

```
   (in-package :cl-user)

   (defvar *last-package* nil)
   (defvar *cached-prompt*)

   (defun my-prompt ()
     (unless (eq *last-package* *package*)
       (setf *cached-prompt*
             (concatenate 'string (or (first (package-nicknames *package*))
                                      (package-name *package*))
                          "> "))
       (setf *last-package* *package*))

   (setf lisp::*prompt* #'my-prompt)
```

If you wish to change the debugger's prompt (which defaults to
something like `0]` , where 0 indicates the number of recursive
debuggers you have entered), there is a similar variable named
`DEBUG:*DEBUG-PROMPT*`. Your function should print a string to
`*DEBUG-IO*`.
