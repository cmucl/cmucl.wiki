<!-- Name: OldNewsPost20e -->
<!-- Version: 1 -->
<!-- Last-Modified: 2014/10/25 12:41:40 -->
<!-- Author: rtoy -->
# Old News from 20e and later

<!-- [[PageOutline]] -->
### Snapshot 2014-09

The [2014-09
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/09/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/09/release-20f.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

* Deprecated `FLOAT-TRAPPING-NAN-P` in favor of
  `FLOAT-SIGNALING-NAN-P`. Signaling NaNs are also now printed with
  "Signaling" instead of "Trapping".
* FP exceptions are handled better on the ppc port.
* The compiler can now derive the type of `ROUND`.
* Fixed an issue with multi-processing destroy-process doesn't take
  effect for a schedulable processbecause it has no run-reason and/or
  arrest-reason.
* Tickets ~~[#105](https://trac.common-lisp.net/cmucl/ticket/105)~~,
  ~~[#84](https://trac.common-lisp.net/cmucl/ticket/84)~~,
  ~~[#101](https://trac.common-lisp.net/cmucl/ticket/101)~~,
  ~~[#106](https://trac.common-lisp.net/cmucl/ticket/106)~~, and
  ~~[#100](https://trac.common-lisp.net/cmucl/ticket/100)~~ have been
  fixed.
* A few issues with fdlibm not signaling FP exceptions have been
  fixed.


### Snapshot 2014-08

The [2014-08
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/08/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/08/release-20f.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

* Updated to ASDF 3.1.3.
* More consistent results for the special functions because all
  platforms now use fdlibm.
* Consing for the trig functions is greatly reduced since we use
  fdlibm instead of a Lisp implementation.
* Source location information has been added for
  `DEFINE-CONDITION`. (From Helmut Eller.)
* The lisp binary is now compiled to use SSE2 on x86 machines. CMUCL
  won't even start now if you do not have SSE2.
* `CLEAR-OUTPUT` will now actually clear any pending output that has
  not been flushed. Previously, `CLEAR-OUTPUT` did nothing.
* For Gray streams, `CLEAR-OUTPUT` will clear output instead of
  calling `STREAM-FORCE-OUTPUT`.
* A critical bug in `KERNEL:DOUBLE-FLOAT-BITS` on x86 has been
  fixed. In some situations, `KERNEL:DOUBLE-FLOAT-BITS` would cause a
  segfault.

### Snapshot 2014-07

There was no snapshot made for this month. 

### Snapshot 2014-06

The 2014-06 snapshot has been released. See the release notes for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* An error in printing `MOST-NEGATIVE-FIXNUM` has been fixed.
* A style warning is now issued when using `T` or `OTHERWISE` in
  `ECASE` and friends. (Ticket
  ~~[#98](https://trac.common-lisp.net/cmucl/ticket/98)~~)

### Snapshot 2014-05

The [2014-05 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/05/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/05/release-20f.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* **Support for x87 has been dropped**. This snapshot and future snapshots will be no longer include support for x87 and will require SSE2 support. 


### Snapshot 2014-04


The [2014-04
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/04/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/04/release-20f.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

* **Support for x87 is being dropped**. This snapshot will be the last snapshot that includes support for x87. All following snapshots will only run on x86 chips that support sse2.
* Type derivation for `log` reverted; `(log -0d0)` must be `-infinity + i*pi` to be able to compute the special functions correctly on the
  branch cuts.
* `(log -0w0)` returns values that correspond to `(log -0d0)`.
* More missing double-double float constants are added like
  `DOUBLE-DOUBLE-FLOAT-POSITIVE-INFINITY`.

### Snapshot 2014-03

The [2014-03
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/03/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/03/release-20f.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

* `RUN-PROGRAM` accepts `:ELEMENT-TYPE` to specify the element type
  for the streams that are created.
* Lisp-unit (a Lisp framework for testing) has been added as a
  contrib. Use `(require :lisp-unit)` to compile and load it. Unlike
  asdf and defsystem, precompild fasls are not included.
* Type derivation for `log` was not consistent with the values
  actually returned by `log`. This is corrected now.
* Double-double floats were missing the constants describing various
  limits such as `LEAST-POSITIVE-DOUBLE-DOUBLE-FLOAT`. These are added
  now.
* When reading very small floats values, the reader will round to
  least-positive float if possible. Otherwise an error is still
  signaled.

### Snapshot 2014-02

The [2014-02
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/02/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/02/release-20f.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

* Micro-optimizations in arithmetic vops for x86/sse2, reducing the
  number of memory loads in some cases.
* Run-time checks for zero pages are now disabled; the lazy zeroing of
  heap pages has been the default for some time without problems, so
  the run-time check is disabled.
* A serious error in the complex multiply vop on sparc has been fixed.

### Snapshot 2014-01

The [2014-01
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/01/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/01/release-20f.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

* The trig functions `sin`, `cos`, and `tan` are faster now and more accurate.
* A bug in `CIS` introduced in the 2013-12 snapshot has been fixed.
* `DISASSEMBLE:DISASSEMBLE-FUNCTION`,
  `DISASSEMBLE:DISASSEMBLE-MEMORY`, and
  `DISASSEMBLE:DISASSEMBLE-CODE-COMPONENT` accept the `:BASE`,
  `:CASE`, and `:RADIX` keyword arguments, like
  `DISASSEMBLE:DISASSEMBLE` does.
* Improved speed for some SSE2 VOPS.
* Values on the branch cuts for the inverse trig and hyperbolic
  functions match the definitions. If there is a difference between
  the CLHS and formulas, we choose the formula.
* Updated support for Mac OSX 10.5 and gcc 4.8 on ppc. 

Due to the bug in `CIS` introduced in the 2013-12 snapshot, the
2013-12 snapshot is deprecated.



### Snapshot 2013-12

The [2013-12
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/12/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/12/release-20f.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

* The compile can derive the result types of `DECODE-FLOAT`.
* `CIS` uses the faster C library routine `sincos` when available.
* `create-target.sh` accepts `-h` and `-?` options to print out some
  help.

### Snapshot 2013-11

The [2013-11
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/11/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/11/release-20f.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

* ASDF 3.0.3.
* Optimizations for x86 (ported from sparc and ppc).
* Numbers will be printed with lower case letters when `:PRINT-CASE`
  is `:DOWNCASE`.

### CMUCL 20e released

[CMUCL 20e](http://common-lisp.net/project/cmucl/downloads/release/20e/) has been released, For information on the changes between 20e and 20d, we refer the reader to the [20e release notes](https://trac.common-lisp.net/cmucl/wiki/Release20e).

Because of the release, there will not be a 2013-10 snapshot.
