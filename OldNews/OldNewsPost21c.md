# Old News from 21c and Later


### Snapshot 2018-03

The [2018-03 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2018/03) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2018/03/release-21d.md) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Gitlab tickets
  * ~~#60~~ The function `C::%UNARY-FROUND` is undefined
  * ~~#58~~ Bogus type error in comparison of complex number with `THE` form
  * ~~#61~~ Segfault when compiling call to `ARRAY-HAS-FILL-POINTER-P` on bit vector constant
  * ~~#62~~ Segfault when compiling `ARRAY-DISPLACEMENT` on a string constant

### Snapshot 2018-02

The [2018-02 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2018/02) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2018/02/release-21d.md) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Changes:
* Updated CLX to latest upstream version
* Gitlab tickets
* Ticket ~~#50~~ CLX (Hemlock) fails to run.
* Ticket ~~#49~~ CLM crashes
* Ticket ~~#47~~ Backquote and multiple splices
* Ticket ~~#59~~ Incorrect type-derivation for `decode-float`

### Snapshot 2018-01

The [2018-01 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2018/01) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2018/01/release-21d.md) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Changes:
* ASDF 3.3.1
* Random number generator changed from MT19937 to xoroshiro128+
  * This new generator is faster and uses significantly fewer bits of state.

### Release 21c

[CMUCL 21c](http://common-lisp.net/project/cmucl/downloads/release/21c/) has been released, For information on the changes between 21c and 21b, we refer the reader to the [release notes](https://gitlab.common-lisp.net/cmucl/cmucl/blob/master/src/general-info/release-21c.md).
