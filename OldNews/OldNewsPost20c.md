<!-- Name: OldNewsPost20c -->
<!-- Version: 1 -->
<!-- Last-Modified: 2012/10/27 17:35:26 -->
<!-- Author: rtoy -->
## Old News from 20c and later

### Snapshot 2012-10

The [2012-10
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/10/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/10/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * `READ-CYCLE-COUNTER` no longer destroys any live values in the
   `EDX` or `ECX` registers.
 * Add `VM::WITH-CYCLE-COUNTER` to return the number of ticks elapsed
   when executing the body. The number of ticks is the number of CPU
   cycles, except for ppc/darwin.


### Snapshot 2012-09

The [2012-09
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/09/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/09/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * ASDF2 updated to version 2.24
 * Add microptimization of `2*x` and `x/(2^n)` when `x` is a float and
   `n` is an integer.
 * Fixed a minor build issue in the CLM debugger. 

### Snapshot 2012-08

The [2012-08
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/08/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/08/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * ASDF2 updated to version 2.23.
 * CMUCL can now be compiled correctly with clang.
 * Fixed a possible stack corruption bug on x86 when calling from C into lisp.
 * Support for Mac OSX 10.4 and earlier has been dropped. (Lack of
   developer access to such machines.)


### Snapshot 2012-07

The [2012-07
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/07/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/07/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * ASDF2 updated to version 2.22.
 * Minor internal changes to support building with clang, which
   doesn't yet work as expected.


### Snapshot 2012-06

The [2012-06
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/06/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/06/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.


 * Implement faster `LOGCOUNT` function on x86, if `:SSE3` feature is available. (Only applies to new uses of LOGCOUNT. The core uses the default version.)
 * On x86, `SET-FLOATING-POINT-MODES` clears any current and accrued
   exceptions that match exceptions in :TRAPS. Previously, enabling a
   trap when the current exception also listed that trap caused the
   exception to be immediately signaled. This no longer happens and
   now matches how ppc and sparc behave.


### Snapshot 2012-05

The [2012-05
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/05/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/05/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * ASDF2 updated to version 2.21.
 * Many additional aliases for external formats added that match the
   glibc iconv aliases.
 * External format for UTF-32 was generating an error when converting
   octets to a string.
 * The UTF-16-BE and UTF-16-LE external formats were returning the
   incorrect number of octets when surrogates pairs were decoded. This
   confuses the stream buffering code.
 * Fix typo in ISO8859-2 external format that caused it not to work
   correctly. This type potentially also caused failures for all other
   external formats that were based on ISO8859-2.
 * Fix ~~[ticket:58](https://trac.common-lisp.net/cmucl/ticket/58)~~:
   UTF-16 buffering problem.


### Snapshot 2012-04

The [2012-04
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/04/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/04/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * Added a new contrib, `contrib-packed-sse2` to allow packed
   operations ala SSE2. Support for some packed sse2 operations like
   multiply, divide, and shuffle for both packed singles and
   doubles. All operations are done on top of `(complex double-float)`
   numbers. Functions are provided to put and get packed
   singles/doubles from `(complex double-float)`.
 * Some VOP costs were incorrect which prevented the fast complex
   double-float multiplier from being used when sse3 is available.
 * Add micro-optimization for x86 (already available on sparc and ppc)
   where `(logand <signed-byte 32> #xffffffff)` generates a register
   move instead of and'ing with `#xffffffff`.


### Snapshot 2012-03

The [2012-03
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/03/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/03/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * Updated to Unicode 6.1.0.
 * `LISP:UNICODE-COMPLETE` no longer signals an error if the prefix
   isn't a prefix of the name of any Unicode character.
 * Add `-R` option to `build.sh` to force recompiling the C runtime.


### Snapshot 2012-02

The [2012-02
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/02/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/02/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * UTF-8 is now build into the core and always available.
 * A bug in `UNICODE-COMPLETE-NAME` has been fixed. (Mostly useful
   with Slime when completing character names.)
 * An issue with blocked signals after an interrupt has been fixed.
 * The ppc port has been revived; only a Unicode version is currently
   available.


### Snapshot 2012-01

The [2012-01
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/01/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/01/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * CMUCL was not printing pathnames like `(make-pathname :directory
   '(:absolute "tmp" "" "/"))` correctly. This is now printed using
   `#P(...)`.


### Snapshot 2011-12

The [2011-12
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/12/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/12/release-20d.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * The directory structure has been changed.
 * ASDF2 updated to version 2.019.
 * Behavior of `STRING-TO-OCTETS` has changed. This is an incompatible
   change from the previous version but should be more useful when a
   buffer is given which is not large enough to hold all the octets
   for the given string. See docstring for more details.
 * `DECODE-FLOAT` was not correctly declared and could not be compiled
   to handle `double-double-float`s.


### **CMUCL 20c released**

[CMUCL
20c](http://common-lisp.net/project/cmucl/downloads/release/20c/) has
been released, For information on the changes between 20c and 20b, we
refer the reader to the [20c release
notes](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/Release20c) or
to the [20c release notes text
file](http://common-lisp.net/project/cmucl/downloads/release/20c/release-20c.txt).

Because of the release, there will not be a 2011-11 snapshot.


