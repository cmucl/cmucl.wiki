# Old News from 20f and later

### Snapshot 2015-09
The
[2015-09 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/09/) has been released. See the
[release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/09/release-21a.txt) 
for details, but here is a quick summary of the
changes between this snapshot and the previous snapshot.

   * Updated ASDF to version 3.1.5
   * Some additional fixes for issue #4 where `ELT` on a list did not
   * Issue #8 fixed: Constants and top-level code
   * On linux, forgot to include `UNIX-GETENV` for asdf and `UNIX-EXECVE`

### Snapshot 2015-07
The
[2015-07 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/07/) has been released. See the
[release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/07/release-21a.txt) 
for details, but here is a quick summary of the
changes between this snapshot and the previous snapshot.

   * Some additional functionality moved back to the core unix.lisp from contrib:
     * `unix-getenv` for asdf
     * `unix-execve` and `unix-fork` for slime.  Thus, slime should
       work without having to `(require :unix)`.
   * `*ERROR-PRINT-LINES*` increased to 10.
   * Issues fixed:
     * ~~#5~~: Give better error messages when using T case in CASE.
     * ~~#4~~: ELT on lists does not signal an error when given invalid index.
     * ~~#3~~: Shadowing compiler macro functions
     * ~~#7~~: Local functions and get-setf-expansions 

### Snapshot 2015-06
The
[2015-06 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/06/) has been released. See the
[release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/06/release-21a.txt) 
for details, but here is a quick summary of the
changes between this snapshot and the previous snapshot.

   * The UNIX package has been changed; it now only contains just
     enough to compile all of cmucl. If you want the rest of old UNIX package, use `(require :unix)` to get that.
   * The clx-inspector contrib module has been added, courtesy of Fred Gilham.
   * ASDF documentation in html, info, and pdf formats is included.
   * Issues fixed:
     * ~~#1~~ fixed.

### Snapshot 2014-12
The
[2014-12 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/12/) has been released. See the
[release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/12/release-21a.txt) 
for details, but here is a quick summary of the
changes between this snapshot and the previous snapshot.

   * Executables on x86 work again. They were accidentally broken when
     we removed support for x86. 
   * PPC now supports executables. Currently executables can be made, and run, but they are rather buggy. 
   * Many fixes the the `LOG` function, including more accurate log2
     and log10 values.
     * `(log (expt 2 n) 2) = n` whenever `(expt 2 n)` would be a valid double-float number.
     * `(log (expt 10 n) 10) = n` whenever `(expt 10 n)` would be a valid double-float number. 
   * New macros: `LISP:WITH-STRING-CODEPOINT-ITERATOR` and `LISP:WITH-STRING-GLYPH-ITERATOR` that work similarly to `WITH-HASH-TABLE-ITERATOR`.
   * Along with the iterators above, `LOOP` includes extensions like
     * `(loop for cp being the codepoint of string ...)`
     * `(loop for g-string being the glyph of string ...)`

### Snapshot 2014-11
The
[2014-11 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/11/) has been released. See the
[release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2014/11/release-21a.txt) 
for details, but here is a quick summary of the
changes between this snapshot and the previous snapshot.


   * ASDF 3.1.4.
   * Fixed issue where `TIME` returned negative cycles on ppc.
   * `:UTF` is no longer an alias for :UTF-8 for external formats.
   * Motifd works as a 64-bit binary (thanks to Richard Kreuter)
   * NetBSD updated to support 64-bit `time_t` and related values (from Robert Swindells). 

A cross-compile on Darwin/x86 is needed for this release. (To get an updated and corrected EXTERN-ALIEN-NAME.)


### CMUCL 20f released

[CMUCL 20f](http://common-lisp.net/project/cmucl/downloads/release/20f/)
has been released, For information on the changes
between 20f and 20e, we refer the reader to the
[release notes](Release20f).

Because of the release, there will not be a 2014-10 snapshot.
