<!-- Name: OldNewsPost20d -->
<!-- Version: 1 -->
<!-- Last-Modified: 2014/02/06 08:39:04 -->
<!-- Author: rtoy -->
# Old News from 20d and Later

### Snapshot 2013-09


The [2013-09
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/09/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/09/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * [DISASSEMBLE](http://www.lispworks.com/documentation/HyperSpec/Body/f_disass.htm)
   is now ANSI compliant; no other arguments are allowed. Use
   `DISAASSEM:DISASSEMBLE` if you need more options.
 * The disassembler now defaults to printing disassembly in base 16
   (without radix marker) and in lower case. If you want other
   options, use `DISASSEM:DISASSEMBLE`.
 * New keyword args for `DISASSEM:DISASSEMBLE`: `base`, `case`,
   `radix` to specify the base, print case, and radix. The defaults
   are 16, `:downcase`, and `NIL`, respectively.
 * Some optimizations in the x86 backend to speed up some
   operations and reduce register restrictions.


### Snapshot 2013-08

The [2013-08
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/08/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/08/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * Improve type-derivation for variables declared as `FLOAT`.
 * Optimize x86 move, length/list, and more-args `VOPS`. 

### Snapshot 2013-07

The [2013-07
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/07/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/07/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

The changes are small for this snapshot, mostly because the main Mac
build machine was updated to Mac OS X 10.8 from 10.6.

 * ASDF 3.0.2 

### Snapshot 2013-06

The [2013-06
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/06/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/06/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * ASDF 3.0.1
 * The Unicode extensions for `CL:STRING-UPCASE` and friends have been
   removed. You can find the extensions in `UNICODE:STRING-UPCASE and
   friends.
 * `REVERSE` on strings is much faster
 * `REVERSE` on strings no longer preserves the order of surrogate
   pairs; they are reversed now, as required.
 * Some fixes to make cross-compiles from x86 to sparc work again.

### Snapshot 2013-05

The [2013-05
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/05/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/05/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * Cleanup of the Config files, including removing outdated or unused Configs.
 * Some speedups in gencgc for most platforms, including Darwin,
   Linux, and Sparc.

### Snapshot 2013-04

The [2013-04
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/04/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/04/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * Fix startup crashes on some Debian Linux versions. This was caused
   by the release string not having a patch version.
 * `FILE-POSITION` no longer returns incorrect values. See ticket ~~[#79](https://trac.common-lisp.net/cmucl/ticket/79)~~.
 * Fix error in `(format t "~ve" 21 5d-324)`. (See ticket
   ~~[#80](https://trac.common-lisp.net/cmucl/ticket/80)~~).

### Snapshot 2013-03-a

Due to a serious error (see ticket
~~[#76](https://trac.common-lisp.net/cmucl/ticket/80)~~ introduced in
2013-02 and persisting in 2013-02, a [new snapshot,
2013-03-a](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/03-a/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/03-a/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * ASDF updated to version 2.32.
 * Update to support Unicode 6.2
 * Ticket ~~[#76](https://trac.common-lisp.net/cmucl/ticket/76)~~ fixed. 
  
### ~~Snapshot 2013-03~~

The [2013-03
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/03/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/03/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * ASDF updated to version 2.30.
 * Attempts to modify the standard readtable or standard pprint
   dispatch table will signal an error.
 * An error in `FILE-POSITION` has been fixed (Ticket ~~[#74](https://trac.common-lisp.net/cmucl/ticket/74)~~).
 * Ticket ~~[#74](https://trac.common-lisp.net/cmucl/ticket/74)~~ fixed. 

### ~~2013-02-20~~

Due to a mistake, the Linux snapshots for 2013-02 were missing support
for alien callbacks. This has been corrected and new binaries have
been uploaded. Look for `cmucl-2013-02-a-x86-linux*` in the snapshot
directory.

### ~~Snapshot 2013-02~~

The [2013-02
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/02/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/02/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * ASDF updated to version 2.28.
 * `DEFINE-COMPILER-MACRO` has source-location information for the
   macro definition.
 * Ticket ~~[#65](https://trac.common-lisp.net/cmucl/ticket/65)~~ fixed.
 * Ticket ~~[#69](https://trac.common-lisp.net/cmucl/ticket/69)~~ fixed.
 * Ticket ~~[#70](https://trac.common-lisp.net/cmucl/ticket/70)~~ fixed.
 * Ticket ~~[#71](https://trac.common-lisp.net/cmucl/ticket/71)~~ fixed.
 * Ticket ~~[#72](https://trac.common-lisp.net/cmucl/ticket/72)~~
   fixed.

In addition, there is a binary for ppc on Mac OSX. Thanks to Toby Thain for providing access to a Mac g5 box. 

### Snapshot 2013-01

The [2013-01
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/01/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2013/01/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * `DEFINE-COMPILER-MACRO` has source-location information for the
   definition.
 * `:ALIEN-CALLBACK` added to `*FEATURES*` for platforms that support
   alien callbacks. This is currently available for all supported
   platforms.
 * `REPLACE` can now handle strings of any supported size.
 * Ticket ~~[#66](https://trac.common-lisp.net/cmucl/ticket/66)~~ fixed
 * Ticket ~~[#67](https://trac.common-lisp.net/cmucl/ticket/67)~~ fixed
 * Ticket ~~[#68](https://trac.common-lisp.net/cmucl/ticket/68)~~ fixed: 
  
### Snapshot 2012-12

The [2012-12
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/12/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2012/12/release-20e.txt)
for details, but here is a quick summary of the changes between this
snapshot and the previous snapshot.

 * ASDF2 updated to 2.26
 * Unicode completion has been reverted to the older version due to
   the inability to complete `#\hangul_syllable_`. Trac
   [ticket:52](https://trac.common-lisp.net/cmucl/ticket/52) reopened.
 * Starting with this snapshot, only the unicode version of CMUCL is
   officially supported. The 8-bit version and code will not be
   deleted, but binaries will no longer be supplied for the 8-bit
   (non-unicode) version.

### ***CMUCL 20d released***
[CMUCL
20d](http://common-lisp.net/project/cmucl/downloads/release/20d/) has
been released, For information on the changes between 20d and 20c, we
refer the reader to the [20d release notes](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/Release20d) or to the [20d release
notes text
file](http://common-lisp.net/project/cmucl/downloads/release/20d/release-20d.txt).

Because of the release, there will not be a 2012-11 snapshot.


