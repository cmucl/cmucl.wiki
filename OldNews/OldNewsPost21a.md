# Old News from 21a and Later

### Snapshot 2016-09

The [2016-09 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/09/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/09/release-21b.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Ticket ~~#25~~ fixed: Issue with ext:run-program and string streams (related to character sizes?)
* Ticket ~~#27~~ fixed: Regression: ASDF test failures
* Ticket ~~#28~~ fixed: Recursive function definition during cross-compile

1In particular, the fix for [#25](/cmucl/cmucl/-/issues/25) means that input and output string streams passed to `RUN-PROGRAM` should be processed by `STRING-ENCODE` and `STRING-DECODE` before handing the string stream to the process and when reading the string back from the process.

### Snapshot 2016-06

The [2016-06 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/06/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/06/release-21b.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Issue ~~#22~~ fixed: Incorrect coercion to float
* The binding stack and control stacks are no longer allocated at fixed memory addresses on x86/linux, x86/darwin, and sparc/solaris. This should not cause any visible changes.

### Snapshot 2016-05

The [2016-05 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/05/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/05/release-21b.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* ASDF updated to version 3.1.7

### Snapshot 2016-03

The [2016-03 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/03/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/03/release-21b.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* The maximum heap (dynamic-space-size) has been reduced on Linux to 1530 MB to accommodate 32-bit Ubuntu 11.10

### Snapshot 2016-02

The [2016-02 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/02/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/02/release-21b.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Handling of floating-point exceptions is improved. Previously, the FP modes were sometimes incorrectly restored and the `ARITHMETIC-ERROR` handler was unable to print FP values.
* A few docstrings have been corrected and updated with more information.

### Snapshot 2016-01

The [2016-01 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/01/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/01/release-21b.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* ASDF 3.1.6.9, to fix an issue with merging `(USER-HOMEDIR-PATHNAME)` with other pathnames.
* Added `EXT:WITH-FLOAT-TRAPS-ENABLED` to complement `WITH-FLOAT-TRAPS-MASKED`.
* `(EXPT 0 POWER)` doesn't throw `INTEXP-LIMIT-ERROR` anymore for any integer value of `POWER`
* Starting cmucl with `-dynamic-space-size 0` now allocates the largest possible heap size for the platform.
* `PATHNAME-MATCH-P` accepts search-lists instead of signaling an error. (See ticket ~~#16~~.)
* Cmucl can now compile and run when compiled using XCode 7.2 (and newer versions of clang). (See ticket ~~#12~~.)

### Snapshot 2015-12

The [2015-12 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/12/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/12/release-21b.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Added `UNIX-SETITIMER` back in for Linux. This was preventing cores from being saved.
* Updated lisp-unit
* Micro-optimization for some modular shifts on x86.

### Snapshot 2015-11

The [2015-11 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/11/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2015/11/release-21b.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Updated ASDF to version 3.1.6
* Support for ASDF static-image-op
* Issue ~~#10~~ fixed: `(setf aref)` for 1, 2, and 4-bit arrays
* Unification of `UNIX` package between linux and all other operating systems.

### Release 21a

[CMUCL 21a](http://common-lisp.net/project/cmucl/downloads/release/21a/) has been released, For information on the changes between 21a and 20f, we refer the reader to the [release notes](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/Release21a).


Because of the release, there will not be a 2015-10 snapshot.