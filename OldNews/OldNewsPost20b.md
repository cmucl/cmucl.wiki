# Old News

Some older news.

### Snapshot 2011-10

The [2011-10
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/10/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/10/release-20c.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * Command-line flags `-read-only-space-size`, `-static-space-size`,
   `-control-stack-size`, and `-binding-stack-size` have been addded
   to control the size of the corresponding region. The default size
   of each is unchanged.
 * Add `-O` option to `build.sh`.
 * The format of the file-comments has changed somewhat. The
   `$Header$` keyword is retained, but the git short SHA1 hash is used
   for the revision.

### 2011-09-20

The CMUCL CVS repository has been converted to git and is now
available. You can browse the repository using the ~Browse Source link
above, or you can visit ~CMUCL gitweb for a different view. This page
also includes links necessary for cloning the repository. (For
commiters, be sure to put your userid in the ssh link.)

The CVS repository will still be available, but will not allow
checkins anymore.

Git is integrated with Trac so the commit messages can refer to and
even close Trac tickets. If the commit messages contains text like
```
        command #1
    	command #1, #2
    	command #1 & #2
    	command #1 and #2
```
then the given command is applied to the specified tickets. Instead of
the short-hand syntax above, you can also use
```
        command ticket:1
    	command ticket:1, ticket:2
    	command ticket:1 & ticket:2
    	command ticket:1 and ticket:2
```
The available commands (not case-sensitive) are:

* **close**, **closed**, **closes**, **fix**, **fixed**, **fixes**:

    The specified issue numbers are closed with the contents of this commit message being added to it.

* **references**, **refs**, **addresses**, **re**, **see**:

    The specified issue numbers are left in their current status, but the contents of this commit message are added to their notes. 

### Snapshot 2011-09

The [2011-09
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/09/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/09/release-20c.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 *   ASDF2 updated to version 2.017.
 *   Improve type propagation for `LOAD-TIME-VALUE`.
 *   Getting documentation of a structure via `DOCUMENTATION` no longer signals an error.
 *   Reduce unnecessary consing of `SAP`s in `ROOM`.
 *   Make stack overflow checking actually work on Mac OS X. The
     implementation had the `:STACK-CHECKING` feature, but it didn't
     actually prevent stack overflows from crashing lisp.
 *   Fix rounding of numbers larger than a fixnum. 

### Snapshot 2011-08
There was no snapshot for this month. 

### Snapshot 2011-07

The [2011-07
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/07/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/06/release-20c.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 *   Unicode database updated to Unicode 6.0.0.
 *   ASDF2 updated to version 2.016.1
 *   Add `LISP:LOAD-ALL-UNICODE-DATA` to load all the unicode
     information into core. This is useful for creating an executable
     image that does not need unidata.bin.
 *   CMUCL no longer exits if you specify a core file with an
     executable image. A warning is printed instead and the core file
     is used.
 *   Trac ~~[#43](https://trac.common-lisp.net/cmucl/ticket/43)~~ has
     been fixed in a better way. The previous fix was incorrect and
     causes some Unicode tests to fail.

### Snapshot 2011-06

The [2011-06
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/06/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/06/release-20c.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * `:CMUCL` is now in `*FEATURES*`
 * Added command line option, `-unidata`, to allow user to specify
   the location and name of the `unidata.bin` file. This is used
   instead of the default location.
 * Opening a file whose name contains "[" with `:IF-EXISTS
   :NEW-VERSION` no longer causes an error.

### Snapshot 2011-05

There was no snapshot in May, partly due to the ongoing upgrade on
common-lisp.net.

### Snapshot 2011-04

The [2011-04
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/04/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/04/release-20c.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * In `COMPILE-FILE`, the second return value is now non-nil if there
   are style warnings. Previously, style warnings were erroneously
   ignored.
 * ASDF has been updated to version 2.014.1 

### Snapshot 2011-03

The [2011-03
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/03/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/03/release-20c.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * In `COMPILE-FILE`, the `:OUTPUT-FILE` can also be a stream. CMUCL
   was erroneously signaling an error..
 * `(OPEN f :DIRECTION :IO :IF-DOES-NOT-EXIST NIL)` no longer signals
   an error if the file `f` does not exist. It returns `NIL` now.
 * In some situations the compiler could not constant fold `SQRT`
   calls because `KERNEL:%SQRT` was not defined on x86 with SSE2. This
   is fixed now.
 * In an earlier snapshot to add support for character name completion
   with [Slime](http://common-lisp.net/project/slime/), a bug was
   introduced where cmucl could no longer read
   `#\latin_small_letter_a`. This is fixed in this version.

### Snapshot 2011-02

The [2011-02
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/02/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/02/release-20c.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * `EXT::DESCRIBE-EXTERNAL-FORMAT` was not exported.
 * `TRACE` was erroneously allowing encapsulation when tracing local
   flet/labels functions. This doesn't actually trace anything. An
   error is now signaled in this case. If you are sure, you can
   specify `:ENCAPSULATE NIL` to disable encapsulation.

### Snapshot 2011-01

The [2011-01
snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/01/)
has been released. See the [release
notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2011/01/release-20c.txt)
for details, but here is a quick summary of the changes between the
this snapshot and the previous snapshot.

 * Initial support for Solaris/x86. CMUCL will run on Solaris/x86 with
   all features available.
 * `UNINTERN` no longer removes the wrong symbol. `UNINTERN` would
   remove the symbol when inherited from another package although it
   should not.
 * `DEFSTRUCT` allows multiple keyword constructors as required by the
   spec.
 * `SUBSEQ` with an end index less than the start index sometimes
   crashes CMUCL. Now, signal an error if the bounds are not valid.
 * Localization support was causing many calls to `stat` trying to
   find non-existent translation files. This has been fixed so that
   the results are cached. (If new translations are added, the cache
   will need to be cleared or cmucl restarted.) This change cuts
   building time by half on Solaris/sparc.
 * On NetBSD, function-end breakpoints, especially for tail-recursive
   functions, are working now.
 * On NetBSD, display of FP numbers (sse2 and x87) during tracing has
   been corrected. Previously, random values were displayed.
 * Executables images can now be created on NetBSD again. 

### 20b patch 000

A critical bug in `REALPART` and `IMAGPART` has been fixed in the
2010-11 snapshot. A
[patch](http://common-lisp.net/project/cmucl/downloads/release/20b/patches/)
is provided now to fix this issue in the 20b release. Installation
instructions are available.

### **CMUCL 20b released**
[CMUCL
20b](http://common-lisp.net/project/cmucl/downloads/release/20b/) has
been released, For information on the changes between 20b and 20a, we
refer the reader to the [20b release
notes](http://common-lisp.net/project/cmucl/downloads/release/20d/release-20b.txt).
