# Old News from 21b and Later


### Snapshot 2017-10

The [2017-10 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2017/10) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2017/10/release-21c.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Changes:
* ASDF 3.3.0
* Bugfixes:
* Fix some compiler warnings and clean up funny indentation in asin code.
* Gitlab tickets:
* Ticket ~~#40~~: Move heap space location for linux
* Ticket ~~#41~~: Report proper process status
* Ticket ~~#44~~: Add docstrings for process accessors

### Snapshot 2017-04

The [2017-04 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2017/04) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2017/04/release-21c.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* ASDF 3.2.1
* FreeBSD 10 binaries are available once again, thanks to Fred Gilham. This includes binaries for [21b](https://common-lisp.net/project/cmucl/downloads/release/21b/) and [snapshot-2017-03](https://common-lisp.net/project/cmucl/downloads/snapshots/2017/03/)

### Snapshot 2017-03

The [2017-03 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2017/03) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2017/03/release-21c.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* ASDF 3.2.0

### Snapshot 2017-01

The [2017-01 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2017/01) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2017/01/release-21c.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Ticket ~~#34~~ fixed: Sparc build fails test issues-tests::issue.25a
* Ticket ~~#38~~ fixed: Don't use unix:unix-times
* `bin/make-dist.sh` options have changed. The positional args for the version, architecture, and os are removed in favor of switches `-V`, `-A`, and `-o`, respectively.

### Snapshot 2016-12

The [2016-12 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/12/) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2016/09/release-21c.txt) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Ticket ~~#36~~ fixed: encode-universal-time signals error
* Ticket ~~#26~~ fixed: The cmucl that never sleeps

### Release 21b

[CMUCL 21b](http://common-lisp.net/project/cmucl/downloads/release/21b/) has been released, For information on the changes between 21b and 21a, we refer the reader to the [release notes](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/Release21b).

