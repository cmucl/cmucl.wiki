# Old News from 21d and Later

### Snapshot 2023-04

The [2023-04 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2023/04) has been released. See the [release notes](https://gitlab.common-lisp.net/cmucl/cmucl/-/releases/snapshot-2023-04) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Changes
  * Update to ASDF 3.3.6
* Bug fixes:
  * ~~#112~~ CLX can't connect to X server via inet sockets.
  * ~~#113~~ REQUIRE on contribs can pull in the wrong things via ASDF.
  * ~~#120~~ `SOFTWARE-VERSION` is implemented in C.
  * ~~#121~~ Wrong column index in FILL-POINTER-OUTPUT-STREAM
  * ~~#122~~ gcc 11 can't build cmucl
  * ~~#124~~ directory with `:wild-inferiors` doesn't descend subdirectories 
  * ~~#125~~ Linux `unix-stat` returning incorrect values
  * ~~#127~~ Linux unix-getpwuid segfaults when given non-existent uid.
  * ~~#128~~ `QUIT` accepts an exit code.
  * ~~#130~~ Move file-author to C.
  * ~~#132~~ Ansi test `RENAME-FILE.1` no longer fails.
  * ~~#134~~ Handle the case of `(expt complex complex-rational)`.
  * ~~#136~~ `ensure-directories-exist` should return the given pathspec.
  * ~~#139~~ `*default-external-format*` defaults to `:utf-8`; add alias for `:locale` external format.
  * ~~#140~~ External format for streams that are not `file-stream`'s.
  * ~~#141~~ Disallow locales that are pathnames to a localedef file.
  * ~~#142~~ `(random 0)` signals incorrect error.
  * ~~#147~~ `stream-line-column` method missing for `fundamental-character-output-stream`.
  * ~~#149~~ Call setlocale(3C) on startup.
  * ~~#150~~ Add aliases for external format cp949 and euckr.
  * ~~#151~~ Change `*default-external-format*` to `:utf-8`.
  * ~~#152~~ Add new external format, `:locale` as an alias to the codeset from LANG and friends.
  #!53 Terminals default to an encoding of `:locale`.
  * ~~#155~~ Wrap help strings neatly.
  * ~~#157~~ `(directory "foo/**/")` only returns directories now.
  * ~~#158~~ Darwin uses utf-8, but we don't support all the rules for pathnames.
  * ~~#162~~ `*filename-encoding*` defaults to `:null` to mean no encoding.
  * ~~#163~~ Add command-line option `-version` and `--version` to get lisp version.
  * ~~#165~~ Avoid inserting NIL into simple `LOOP` from `FORMAT`.
  * ~~#166~~ Fix incorrect type declaration for exponent from `integer-decode-float`.
  * ~~#167~~ Low bound for `decode-float-exponent` type was off by one.
  * ~~#168~~ Don't use negated forms for jmp instructions when possible.
  * ~~#169~~ Add pprinter for `define-vop` and `sc-case`.
  * ~~#172~~ Declare `pathname-match-p` as returning `nil` or `pathname`.
  * ~~#173~~ Add pprinter for `define-assembly-routine`.
  * ~~#176~~ `SHORT-SITE-NAME` and `LONG-SITE-NAME` return `NIL`.
  * ~~#177~~ Add pprinter for `deftransform` and `defoptimizer`.
  * ~~#192~~ Print radix marker in disassemblies and adjust note column to be larger for x86.
  * ~~#193~~ Treat `NIL` and `:UNSPECIFIC` as equivalent when comparing pathnames with `equal`.

### Snapshot 2021-07

The [2021-07 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2021/07) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2021/07/release-21e.md) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Gitlab tickets
  * ~~#87~~ Fixes stepping through the source forms in the debugger. This has been broken for quite some time, but it works now.
  * ~~#97~~ Define and use ud2 instruction isntead of int3. Fixes single-stepping.
  * ~~#103~~ RANDOM-MT19937-UPDATE assembly routine still exists
  * ~~#107~~ Replace u_int8_t with uint8_t
  * ~~#108~~ Update ASDF version 3.3.5

### Snapshot 2021-01

The [2021-01 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2021/01) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2021/01/release-21e.md) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Gitlab tickets
  * ~~#86~~ Building with gcc 8 and later works when using -O2 optimization
  * ~~#90~~ Some static symbols have been removed. This probably makes the fasl files incompatible with older versions.
  * ~~#91~~ Loop destructuring no longer incorrectly signals an error
  * ~~#95~~ Disassembler syntax of x86 je and movzx is incorrect
  * ~~#98~~ fstpd is not an Intel instruction; disassemble as `fstp dword ptr [addr]`
  * ~~#100~~ ldb prints out unicode base-chars correctly instead of just the low 8 bits.

### 2020-11-28

The sparc port is now up-to-date and the [2020-04 snapshots](http://common-lisp.net/project/cmucl/downloads/snapshots/2020/04) have been uploaded.

### Snapshot 2020-04

The [2020-04 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2020/04) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2020/04/release-21e.md) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Changes
  * Updated to ASDF 3.3.4
* Gitlab tickets
  * ~~#79~~ Autoload ASDF when calling `REQUIRE` the first time. User's no longer have to explicitly load ASDF anymore.
  * ~~#80~~ Use ASDF to load contribs. cmu-contribs still exists but does nothing. The contrib names are the same, except it's best to use a keyword instead of a string. So, `:contrib-demos` instead of `"contrib-demos"`.
  * ~~#81~~ Added contribs from Eric Marsden

### Snapshot 2019-06

The [2019-06 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2019/06) has been released. See the [release notes](http://common-lisp.net/project/cmucl/downloads/snapshots/2019/06/release-21d.md) for details, but here is a quick summary of the changes between this snapshot and the previous snapshot.

* Changes
  * Updated to ASDF 3.3.3
* Gitlab tickets
  * ~~#73~~ Update clx to upstream sharplispers/clx
  * ~~#68~~ Can't build with Fedora 28 and gcc 8.1.1

### Release 21d

[CMUCL 21d](http://common-lisp.net/project/cmucl/downloads/release/21d/) has been released, For information on the changes between 21d and 21c, we refer the reader to the [release notes](https://gitlab.common-lisp.net/cmucl/cmucl/blob/master/src/general-info/release-21d.md).
