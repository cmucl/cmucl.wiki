# Project history & Who's Who

CMUCL was developed at the Computer Science Department of [Carnegie Mellon University](http://www.cs.cmu.edu/). The work was a small autonomous part within the [Mach microkernel-based operating system project](http://www.cs.cmu.edu/afs/cs.cmu.edu/project/mach/public/www/mach.html), and started more as a tool development effort than a research project. The project started out as Spice Lisp, which provided a modern Lisp implementation for use in the CMU community. CMUCL has been under continual development since the early 1980's (concurrent with the Common Lisp standardization effort). Most of the CMU Common Lisp implementors went on to work on the [Gwydion environment for Dylan](http://www.cs.cmu.edu/afs/cs/project/gwydion/docs/htdocs/gwydion/). The CMU team was lead by [Scott E. Fahlman](http://www-cgi.cs.cmu.edu/cgi-bin/finger?sef%2B@cs.cmu.edu), the Python compiler was written by [Robert MacLachlan](http://www-cgi.cs.cmu.edu/cgi-bin/finger?ram%2B@cs.cmu.edu).

CMUCL's CLOS implementation is derived from the PCL reference implementation written an Xerox PARC. Its implementation of the LOOP macro was derived from code from Symbolics, which was derived from code written at MIT. The CLX code is copyrighted by Texas Instruments Incorporated.

CMUCL was funded by DARPA under CMU's "Research on Parallel Computing" contract. Rather than doing pure research on programming languages and environments, the emphasis was on developing practical programming tools. Sometimes this required new technology, but much of the work was in creating a Common Lisp environment that incorporates state-of-the-art features from existing systems (both Lisp and non-Lisp). Archives of the project are [available online](ftp://ftp.cs.cmu.edu/afs/cs.cmu.edu/project/clisp/).

The project funding stopped in 1994, so support at Carnegie Mellon University has been discontinued. The project continues as a group of users and developers collaborating over the Internet. Past and present contributors are:

|                  |                                     |
|------------------|-------------------------------------|
| Martin Cracauer  | (cracauer@cons.org)          |
| Helmut Eller     |                                     |
| Peter Van Eynde  | (pvaneynd@debian.org)        |
| Paul Foley       |                                     |
| Fred Gilham      |                                     |
| Alex Goncharov   |                                     |
| Rob MacLachlan   | (ram+@CS.cmu.edu)            |
| Pierre Mai       | (pmai@pmsf.de)               |
| Eric Marsden     | (emarsden@laas.fr)           |
| Gerd Moellmann   | (gerd.moellmann@t-online.de) |
| Timothy Moore    | (moore@bricoworks.com)       |
| Jan Rychter      |                                     |
| Carl Shapiro     |                                     |
| Robert Swindells |                                     |
| Raymond Toy      | (raymond.toy@stericsson.com) |
| Paul Werkowski   | (pw@snoopy.mv.com)           |

This list is not exhaustive. We certainly appreciate everyone who has ever contributed in any way to CMUCL, including bug fixes, new features, and discussions.

New contributors are always welcome; you should join the cmucl-imp mailing list, and read the [projects page](projects.html). A more complete list of the people who have contributed to CMUCL over time is available in the *CMUCL User's Manual*.

If you are using CMUCL in an academic project, you can [cite the project](http://citeseer.nj.nec.com/context/36353/0) by referencing the *CMUCL User's Manual*. Here is a [BiBTeX entry](http://liinwww.ira.uka.de/cgi-bin/bibshow?e=Pt0vojy/3/vojrvf%7d378243&r=bibtex) for the CMU Technical Report.
