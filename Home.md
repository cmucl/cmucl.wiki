# Welcome to the CMUCL Wiki

`CMUCL` is a free implementation of the [Common Lisp](http://en.wikipedia.org/wiki/Common_Lisp) programming language which runs on most major Unix platforms. It mainly conforms to the [ANSI Common Lisp standard](http://www.lispworks.com/documentation/HyperSpec/Front/index.htm). Here is a summary of its main features:

* support for **static arrays** that are never moved by GC but are properly removed when no longer referenced.
* **Unicode** support, including many of the most common external formats such as UTF-8 and support for handling Unix, DOS, and Mac end-of-line schemes.
* native **double-double floats** including complex double-double floats and specialized arrays for double-double floats and and complex double-double floats that give approximately 106 bits (32 digits) of precision.
* a **sophisticated native-code compiler** which is capable of powerful type inferences, and generates code competitive in speed with C compilers.
* **generational garbage collection** on all supported architectures.
* **multiprocessing capability** on the x86 ports.
* a foreign function interface which allows interfacing with C code and system libraries, including shared libraries on most platforms, and direct access to Unix system calls.
* support for interprocess communication and remote procedure calls.
* an implementation of CLOS, the [Common Lisp Object System](http://en.wikipedia.org/wiki/Common_Lisp_Object_System), which includes multimethods and a metaobject protocol.
* a graphical source-level debugger using a Motif interface, and a code profiler.
* an interface to the X11 Window System (CLX), and a sophisticated graphical widget library ([Garnet](https://www.cs.cmu.edu/\~garnet/), available separately).
* programmer-extensible input and output streams ([Gray Streams](http://www.nhplace.com/kent/CL/Issues/stream-definition-by-user.html) and [simple-streams](http://www.franz.com/support/documentation/current/doc/streams.htm)).
* an Emacs-like editor, [Hemlock](http://cmucl.org/hemlock/index.html), implemented in Common Lisp.
* **freely redistributable**: free, with full source code (most of which is in the public domain) and no strings attached (and no warranty). Like the GNU/Linux and \*BSD operating systems, CMUCL is maintained and improved by a team of volunteers collaborating over the Internet.

## Obtaining Cmucl
First, get the tar files as described in [GettingCmucl](GettingCmucl.md).
Then you need to install them, as instructed in
[InstallingCmucl](InstallingCmucl).

## Latest News

### Snapshot 2024-08
The [2024-08 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2024/08) has been released.  See the [release notes](https://gitlab.common-lisp.net/cmucl/cmucl/-/releases/snapshot-2024-08/release-21f.md)
for full details, but here is a quick summary of the changes between
this snapshot and snapshot 2023-08.

* Tickets fixed:
  * #303 Variable `*assert-not-standard-readtable*` defined but not used.
  * #312 Compiler error building motif server on Fedora 40
  * #314 tanh incorrect for large args
  * #316 Support roundtrip character casing
  * #320 Motif variant not defaulted for `x86_linux_clang` config
  * #321 Rename Motif Config.x86 to Config.linux
  * #323 Make string casing functions compliant
  * #329 Fix compiler warnings in os.lisp
  * #330 Fix typos in unicode.lisp
  * #333 `load` doesn't accept generalized boolean for `:if-does-not-exist` arg

Note that #316 is an incompatible change.  Character casing now
follows the CL spec so that you can roundtrip the case and end up with
the same character.  Thus, converting to uppercase and the lowercase
(and vice versa) produces the original character.  Previously, cmucl
followed Unicode which didn't always have characters roundtrip when
doing casing.  (Some characters have 3 cases:  lower, upper, title.) 


### Snapshot 2024-04
The [2024-04 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2024/04) has been released.  See the [release notes](https://gitlab.common-lisp.net/cmucl/cmucl/-/releases/snapshot-2024-04/release-21f.md)
for full details, but here is a quick summary of the changes between
this snapshot and snapshot 2023-08.

* Feature enhancements
  * Add support for Gray streams implementation of file-length via
   `ext:stream-file-length` generic function.

* Changes:
  * Update to ASDF 3.3.7
  * The RNG has changed from an old version of xoroshiro128+ to
    xoroshiro128**.  This means sequences of random numbers will be
    different from before.  See #276.

* Tickets fixed:
  * #171 Readably print `(make-pathname :name :unspecfic)`
  * #180 Move `get-page-size` to C
  * #252 Add script to run ansi-tests
  * #245 Replace `egrep` with `grep -E` in `make-dist.sh`
  * #256 loop for var nil works
  * #259 `system::*software-version*` undefined when compiling
    on linux
  * #260 Command line options `-edit` and `-slave` no longer
    available for Hemlock
  * #261 Remove `get-system-info` from "bsd-os.lisp"
  * #268 Can't clone ansi-test repo on Mac OS CI box
  * #265 CI for mac os is broken
  * #266 Support "~user" in namestrings
  * #269 Add function to get user's home directory
  * #270 Simplify `os_file_author` interface
  * #271 Update ASDF to 3.3.7
  * #272 Move scavenge code for static vectors to its own function
  * #274 1d99999999 hangs
  * #275 FP underflow in reader allows restarting with 0
  * #276 Implement xoroshiro128** generator for x86
  * #277 `float-ratio-float` returns 0 for numbers close to
    least-positive-float
  * #278 Add some more debugging prints to gencgc
  * #283 Add VOP for `integer-length` for `(unsigned-byte 32)` arg.
  * #284 Microoptimize `signed-byte-32-int-len` VOP for x86.
  * #288 Re-enable `deftransform` for random integers.
  * #290 Pprint `with-float-traps-masked` better
  * #291 Pprint `handler-case` neatly.
  * #293 Allow restarts for FP overflow in reader.
  * #294 Implement assembly routine for xoroshiro update function
  * #296 Disassembly of movd instruction broken
  * #297 Pprint `new-assem:assemble` with less indentation.
  * #298 Add `with-float-rounding-mode` macro
  * #299 Enable xoroshiro assembly routine


### Snapshot 2023-08
The [2023-08 snapshot](http://common-lisp.net/project/cmucl/downloads/snapshots/2023/08) has been released.  See the [release notes](https://gitlab.common-lisp.net/cmucl/cmucl/-/releases/snapshot-2023-08)
for full details, but here is a quick summary of the changes between
this snapshot and 21e.

- Gitlab tickets:
    - ~~#154~~ piglatin translation does not work anymore
    - ~~#171~~ Readably print (make-pathname :name :unspecfic)
    - ~~#196~~ Fix issues with mapping and nconc accumulation (mapcan)
    - ~~#216~~ enough-namestring with relative pathname fails
    - ~~#234~~ Make :ASCII external format builtin
    - ~~#240~~ Speed up set operations
    - ~~#242~~ Fix bug in alien-funcall with c-call:char as result type
    - ~~#244~~ Add c-call:signed-char
    - ~~#248~~ Print MOVS instruction with correct case
    - ~~#249~~ Replace LEA instruction with simpler shorter instructions in arithmetic vops for x86
    - ~~#253~~ Block-compile list-to-hashtable and callers
    - ~~#258~~ Remove get-page-size from linux-os.lisp

### Release 21e
[CMUCL
21e](http://common-lisp.net/project/cmucl/downloads/release/21e/) 
has been released.  For information on the changes between 21e and
21d, we refer the reader to the [release
notes](https://gitlab.common-lisp.net/cmucl/cmucl/-/releases/21e).

### Older News

  - [21d](OldNews/OldNewsPost21d)
  - [21c](OldNews/OldNewsPost21c)
  - [21b](OldNews/OldNewsPost21b)
  - [21a](OldNews/OldNewsPost21a)
  - [20f](OldNews/OldNewsPost20f)
  - [20e](OldNews/OldNewsPost20e)
  - [20d](OldNews/OldNewsPost20d)
  - [20c](OldNews/OldNewsPost20c)
  - [20b](OldNews/OldNewsPost20b)
  - [Much older news](http://www.cmucl.org/news/index.html)

## Additional Information

The official web presence is at [www.cmucl.org](http://www.cmucl.org)
with a mirror at [common-lisp.net/project/cmucl/mirror](http://common-lisp.net/project/cmucl/mirror). There may be some duplication between the wiki and the web pages; the wiki tends to be more up-to-date.

* [CMUCL FAQ](FAQS)
* [Developer Information](Develooper)
* [TipsAndTricks](FAQS/TipsAndTricks)
* [CmuclDocumentation](CmuclDocumentation)
* [ReleaseNotes](ReleaseNotes/ReleaseNotes)
