<!-- Name: Release19d -->
<!-- Version: 1 -->
<!-- Last-Modified: 2014/09/02 21:23:01 -->
<!-- Author: rtoy -->
========================== C M U C L  19 d =============================

The CMUCL project is pleased to announce the release of CMUCL 19d.
This is a major release which contains numerous enhancements and
bugfixes from the 19c release.

CMUCL is a free, high performance implementation of the Common Lisp
programming language which runs on most major Unix platforms. It
mainly conforms to the ANSI Common Lisp standard. CMUCL provides a
sophisticated native code compiler; a powerful foreign function
interface; an implementation of CLOS, the Common Lisp Object System,
which includes multimethods and a metaobject protocol; a source-level
debugger and code profiler; and an Emacs-like editor implemented in
Common Lisp. CMUCL is maintained by a team of volunteers collaborating
over the Internet, and is mostly in the public domain.

New in this release:


  * Feature enhancements:
	- A DOUBLE-DOUBLE-FLOAT uses two DOUBLE-FLOAT's to represent a
	  number with >= 106 bits of precision (about 33 digits).
	-  Known issues:
	  - If you are expecting IEEE-style behavior, you don't get it:
	    - signed zeroes aren't really available.  
	    - overflows don't return infinity but return NaN instead.
	    - rounding might not be quite the same as IEEE
	    - SQRT is not accurate to the last bit, as required by IEEE.
	  - Multiplying by a number very close to
	    most-positive-double-float will produce an error even if the
	    result does not overflow.  (This is an artifact of how
	    multiplication is done.  I don't have a solution to this.)
	  - Read/write consistency is not working.  (Because conversion
	    from a bignum to a double-double-float doesn't really
	    understand the internal double-double-float format.)
	  - INTEGER-DECODE-FLOAT and SCALE-FLOAT aren't "inverses".
	    That is, you can't take the result of integer-decode-float
	    and use scale-float to produce exactly the same number.
	    This is because of how bignums are converted to
	    double-doubles.
	  - FLOAT-DIGITS always returns 106 even though there could be
	    more bits.  (Consider the double-double (1d0,1d-200)).  This
	    will show up in PRINT where the printed result will have way
	    more than the normal 33 digits or so.  But reading such a
	    number back won't give the same value.
	  - There is probably more consing than is necessary in many of
	    the standard Common Lisp functions like floor, ffloor, etc.
	  - The special functions are not fully tested.  I did a few
	    random spot checks for each function and compared the
	    results with maxima to verify them.
	  - The branch cuts for the special functions very likely will
	    not match the double-float versions, mostly because we don't
	    have working signed zeroes.
	  - Type derivation for double-double-floats might not be
	    working quite right.
	  - PI is still a double-float.  If you want a double-double
	    version of pi, it's KERNEL:DD-PI.  (Soon to be EXT:DD-PI.)
	  - There are probably still many bugs where double-double-float
	    support was overlooked.
	  - The double-double arithmetic operations can be inlined by
	    specifying (SPACE 0).  Otherwise, they are not inlined.
	    (Each double-double operation is about 20 FP instructions.)

	    :KEY
		The entry exists as long as the key is not
		garbage-collected.
		The entry exists as long as the value is not
		garbage-collected.
		The entry exists as long as the key and the value are
		alive.
	        The entry exists as long as the key or the value are alive.
  * Numerous ANSI compliance fixes:
  * Numerous bugfixes:
  * Trac Tickets
	 - When an entry is freed in a weak hash-table, the entry is
	   actually marked as free now.  Previously, MAPHASH and
	   WITH-HASH-TABLE-ITERATOR would still display (potentially
	   incorrect) entry for it.
  * Other changes:
  * Improvements to the PCL implementation of CLOS:
  * Changes to rebuilding procedure:

This release is not binary compatible with code compiled using CMUCL
19c; you will need to recompile FASL files. 

See <URL:http://www.cons.org/cmucl/> for download information,
guidelines on reporting bugs, and mailing list details.


We hope you enjoy using this release of CMUCL!

