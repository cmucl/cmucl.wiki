<!-- Name: Release18e -->
<!-- Version: 2 -->
<!-- Last-Modified: 2012/08/11 16:11:08 -->
<!-- Author: rtoy -->
# ========================== C M U C L  18 e =============================

The CMUCL project is pleased to announce the release of CMUCL 18e.
This is a major release which contains numerous enhancements and
bugfixes from the 18d release.

CMUCL is a free, high performance implementation of the Common Lisp
programming language which runs on most major Unix platforms. It
mainly conforms to the ANSI Common Lisp standard. CMUCL provides a
sophisticated native code compiler; a powerful foreign function
interface; an implementation of CLOS, the Common Lisp Object System,
which includes multimethods and a metaobject protocol; a source-level
debugger and code profiler; and an Emacs-like editor implemented in
Common Lisp. CMUCL is maintained by a team of volunteers collaborating
over the Internet, and is mostly in the public domain.

New in this release:
  * Feature enhancements:

  * Numerous ANSI compliance fixes:

  * Numerous bugfixes:

  * Other changes:

  * Numerous improvements to the PCL implementation of CLOS:

  * Improvements to Hemlock, the Emacs-like editor:

  * Changes to rebuilding procedure:
  * Deprecated features:
  

This release is not binary compatible with code compiled using CMUCL
18d; you will need to recompile FASL files. 

See <URL:http://www.cons.org/cmucl/> for download information,
guidelines on reporting bugs, and mailing list details.


We hope you enjoy using this release of CMUCL!

