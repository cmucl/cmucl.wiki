<!-- Name: Release18c -->
<!-- Version: 3 -->
<!-- Last-Modified: 2014/10/12 22:27:57 -->
<!-- Author: rtoy -->
========================== C M U C L  18 c =============================

December 25, 2000

The CMUCL project is pleased to announce the release of CMUCL 18c.
This is a major release which contains numerous enhancements and
bugfixes from the 18b release.

CMUCL is a free, high performance implementation of the Common Lisp
programming language which runs on most major Unix platforms. It
mainly conforms to the ANSI Common Lisp standard. CMUCL provides a
sophisticated native code compiler; a powerful foreign function
interface; an implementation of CLOS; the Common Lisp Object System;
which includes multimethods and a metaobject protocol; a source-level
debugger and code profiler; and an Emacs-like editor implemented in
Common Lisp. CMUCL is maintained by a team of volunteers collaborating
over the Internet, and is in the public domain.

Features new in this release:
   * Every platform now supports `(complex single-float)` and
   * Every platform now supports signed-array's with elements of type
   * On the x86 and sparc platform, much larger heaps are available.
   * Support for glibc2 on x86.
   * Support for FreeBSD 4.x, Solaris 2.7 & 8.
   * Better support for Sparc V9 machines (UltraSparc II and later).
   * Better handling of logical pathnames and pathnames in general.
   * equalp hash tables implemented.
   * New implementations of some sequence functions to run significantly
   * Specialized arrays and various other objects can be printed readably
   * `LOCALLY` is a special form, not a macro.
   * `COMPILER-LET` is now in the extension package.
   * `PRINT-OBJECT` methods can be used with structures.  (But the
   * The dynamic heap size is now controllable from the command line with
   * The {{[-batch``` commandline option flushes all output on exit, and
   * A new `-quiet` commandline to suppress informational messages.
   * `:cmu17` is no longer a `*feature*`.  This may break existing code.  A

Some sparc-specific changes:

   * Bigger spaces:
   * Added VOPS for complex single/double-float operations.
   * Try not to use deprecated instructions in the V9 architecture.
   * In certain situations, use conditional move instructions available
   
And finally,

   * Lots and lots of bug fixes, improvements to ANSI CL conformance


This release is not binary compatible with code compiled using CMUCL
18b; you will need to recompile FASL files. 

See <URL:http://www.cons.org/cmucl/> for download information, guidelines
on reporting bugs, and mailing list details.


We hope you enjoy using this release of CMUCL!
