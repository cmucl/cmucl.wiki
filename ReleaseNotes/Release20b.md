<!-- Name: Release20b -->
<!-- Version: 2 -->
<!-- Last-Modified: 2012/06/15 10:34:11 -->
<!-- Author: rtoy -->
# ========================== C M U C L  20 b =============================

The CMUCL project is pleased to announce the release of CMUCL 20b.
This is a major release which contains numerous enhancements and
bug fixes from the 20a release.

CMUCL is a free, high performance implementation of the Common Lisp
programming language which runs on most major Unix platforms. It
mainly conforms to the ANSI Common Lisp standard. CMUCL provides a
sophisticated native code compiler; a powerful foreign function
interface; an implementation of CLOS, the Common Lisp Object System,
which includes multi-methods and a meta-object protocol; a source-level
debugger and code profiler; and an Emacs-like editor implemented in
Common Lisp. CMUCL is maintained by a team of volunteers collaborating
over the Internet, and is mostly in the public domain.

New in this release:

  * Known issues:

  * Feature enhancements:

  * ANSI compliance fixes:

  * Bugfixes:

  * Trac Tickets

  * Other changes:

  * Improvements to the PCL implementation of CLOS:

  * Changes to building procedure:

This release is not binary compatible with code compiled using CMUCL
20a; you will need to recompile FASL files. 

See <http://www.cons.org/cmucl/> for download information,
guidelines on reporting bugs, and mailing list details.


We hope you enjoy using this release of CMUCL!

