# Release notes for CMU Common Lisp 14c, 6 June 91

  **The FASL file format has changed, so all files must be recompiled.**

These notes describe changes since the beta release of 3 February 91.  This is
the first CMU CL release to run on Mach SPARCs as well as on PMAXen (DECstation
3100 or 5000).  Version 14c will go out to both beta and default, since there
is currently no default release.

This release has a substantial space reduction due to compiling with debug-info
1 and reduced safety.  The core is currently 19.2 meg, which is 7 meg smaller
than the last beta release (despite added functionality.)

Major parts of the system are now compiled with no error checking.  Users
should not notice any reduction in safety, since user visible interfaces are
supposed to be fully checked.  Standard functions that users can cause to get
unbound symbol or array index errors needed to be changed to either do explicit
error checks or locally use a safe policy.  Some of these cases may have been
missed.  Let us know if you get any less-than-informative error messages
(segmentation violation) inside standard functions.


## New packages:

The X based graphical inspector is now available.  It now uses standard
fonts (courier) and has a bigger help window.

An improved version of the profile package (previously in the library) is now
in the core.  It now compensates for recursive calls or nested calls, and
interacts better with TRACE and function redefinition.  The old profile
documentation is in:
    /afs/cs/project/clisp/library/profile/profile.doc


## Code:

Argument type checking for Common Lisp functions is now driven by the
compiler's function type database (the types reported by DESCRIBE.)  This means
that some type errors might be detected that were previously unnoticed.

Changed the internal `WITH-ARRAY-DATA` macro to do bounds checking.  This causes
various string functions to give better error messages when an `:END` arg is out
of bounds or `:START` is greater than `:END`.

## Tuning:
Some tuning in `SYSTEM:SERVE-EVENT` which reduces consing and speeds up
Hemlock and terminal I/O.

Changed `GET-INTERNAL-REAL-TIME` to subtract out the time of the first
call to minimize the probability of bignum results.  Also some other tuning
that reduced the consing of this function to 0.

Tuned bignum code and added declarations to reduce number consing.


## DEFSTRUCT:
Fixed default-structure-print to work when *print-circle* is T.
Merged fix to `DEFSTRUCT` constructor parsing that allows multiple default
constructors, or none at all.

## Merged bug fixes from old RT system:
`STANDARD-CHAR-P` no longer returns T for #\return.

Fixed a bug in format regarding ~@*.

Fixed the read-eval-print loop to frob +, ++, +++ correctly.

Fixed a bug in `Y-OR-N-P`.  It was calling `WHITESPACEP` on a symbol.

Fixed `READ-QUOTE` to call `READ` with t for eof-errorp which it previously
failed to do.  fixed `READ-PRESERVING-WHITESPACE` to no longer screw with
eof-errorp based on recursivep

## Package system:
Changed `DEFPACKAGE` to expand into stuff that will have the package effect
at compile time as well as at load time.

Fixed `DEFPACKAGE` to deal more correctly with finding symbols that must
exist.

Fixed package system code to not destructively modify the `USE`, `USED-BY` and
`SHADOWING-SYMBOLS` lists so that they don't get retroactively modified when
we hand them off to the user.

Also, in `SHADOW`, when symbols is `NIL`, shadow no symbols, not `NIL`.

Fixed a bug in `RENAME-PACKAGE` that happened when the new name was one of
the old nicknames.

## Streams:
Tweaked handling of `LISTEN` a bit to allow `READ-CHAR-NO-HANG` to work
correctly.  Fixed the listen method for concatenated streams.  It failed to
step to the next stream when the current one hit eof.

Make two-way streams force-output on the output side before passing any
input requests on to the input side.  Made *standard-output* a two-way
stream so that reading *standard-input* will force output on standard
output.  This eliminates the need for explicit calls for `FORCE-OUTPUT` when
prompting.

Some tuning and bug fixes to `FD-STREAMS` (file descriptor streams) which are
used for file I/O (and now for communication with the X server.)  Also, now
`OPEN` complains if you try to open a non-writable file for output with
`:RENAME` or `:RENAME-AND-DELETE`.  Previously this would succeed as long as
the directory was writable.  `SYSTEM:READ-N-BYTES` on FD streams is now more
efficient, but does *not* wait using `SERVE-EVENT`; it blocks instead.

## TRACE:
Use `FORCE-OUTPUT` instead of `FINISH-OUTPUT` to prevent gratuitous slowdowns
when running in a slave.

If we enter trace recursively (due to the printer calling the traced
function), then just quietly call the function, instead of signalling an
annoying "unable to trace" error.

## LOAD:
Changed load to look at the file contents for the "FASL FILE" header to
determine whether to fasload or slow load, instead of forcing use of a
single fasl file type.  Also, when the given filename doesn't exist and
doesn't have a type, try ``fasl'' in addition to the machine specific fasl
file type.  Eliminated the "feature" whereby zero-length fasl files were
considered to be valid (doing nothing).  Now if you try to load a file with
a fasl file type, but that doesn't have a valid fasl header, then you will
get an error (proceeding loads as a source file.)

When the loader prints comments about loading progress, the number of
leading semicolons is now the depth of recursive loading.

Added a `CONTINUE` restart in `LOAD` that returns `NIL`. 

## GC:
Fixed some bugs in control of garbage collection that should solve some
problems with GC failing to be triggered automatically.  Also, GC no longer
implicitly reenables automatic GC if it has been disabled with `GC-OFF`.

Changed the default GC notify function to not beep.  The old behavior can
still be obtained by setting `*GC-VERBOSE*` to `:BEEP`.  Note that this only
affects use on TTYs, since slave GC notification works differently.

Wrapped a without-interrupts around the guts of maybe-gc so that the notify
messages and state updates don't get seperated from the actual gc.

Removed the icache flushing stuff from GC, because it was unneeded (and
sometimes printed annoying messages that it didn't work).

## X3J13 cleanups:
The non-destructive string functions now accept characters as well as
strings and symbols.

`MACROEXPAND` now expands symbol macros.

Now almost all Common Lisp functions which are `SETF`able have a `(SETF name)`
function.  The exceptions are functions where it makes no sense (`LDB`,
`GETF`), and a few other functions (`GET`, `GETHASH`.)  Now `SETF` of `APPLY` works
for any function which has a setf function.

Changed `GET-SETF-METHOD` to ignore setf macros (always global) when there is
a local macro or function of the place function.  [An x3j13 cleanup]

Fixed the `LOOP` `THEREIS` keyword.  Changed a null test in `LOOP` into an endp
test.

## Other bug fixes:
Fixed sequence functions with output type specifiers to handle `DEFTYPE`'ed
types and other complex types correctly.  (`COERCE` still can't hack
`DEFTYPE`s, though.)

Fixed typep of `(satisfies (lambda (obj) ...))` to coerce the form into a
function so that "object not function" errors don't result.

Fixed `DOCUMENTATION` to return only one value.

## Enhancements:
`ROOM` is now much more verbose, displaying a breakdown of memory usage by
object type.

Changed the printer to print the name of code objects and the value of
value cells.


## CLX:

Modified `EXT:OPEN-CLX-DISPLAY` to set `XLIB:DISPLAY-DEFAULT-SCREEN` to the
screen specified by the user before returning the display.

Merged in a bug-fix to `EVENT-LISTEN` to make it return the right number of
events when called when there is a current event (i.e. in an `EVENT-CASE`.)

Fixed the CLX X interface to be much more efficient, as well as fixing some
bugs.  The low-level I/O to the server is now faster and conses much less.
Enabled some code speeds up pixarray read/write (though it could still be much
better.)  Also, eliminated redundant type checking and fixed some broken
declarations.  This fixes problems with CLX sometimes not working with some X
servers (like the RT server.)


## Compiler:

### Bug fixes:
Fixed incorrect argument type information for some standard Common Lisp
functions.

Fixed `PROCLAIM` to work correctly when the argument isn't a constant.

Fixed the `DEBUG` optimize quality to be called `DEBUG` instead of `DEBUG-INFO`.

Fixed the compiler to not flame out if it sees a `SATISFIES` type specifier
where the predicate function is undefined, and generally to deal better
with testing whether a compile-time constant is of some type that may not
be properly defined yet.

Fixed a number of bugs in the handling of closures over top-level
variables.

Fixed a problem with semi-inline functions.

The compiler note count is no longer incremented when notes are suppressed
by `INHIBIT-WARNINGS` 3.

Some fixes that should eliminate spurious undefined-function warnings.  In
particular, definitions of functions in non-null lexical environments will
be noticed.

Also, now if a function is defined incompatibly with previous calls, the
warning will have proper source context.

Fixed a bug in accessors for 1,2, and 4 bit arrays that was causing #* to
generate incorrect bit vectors.

Changed the type system to consider #(:foo :bar) to be a subtype of 
(vector keyword).  In other words, array subtype relations are determined
according to the specialized element types actually present in this
implementation, rather than assuming that all element types can be
discriminated.

Fixed a problem that could cause type checks to be spuriously deleted in
some contexts where there is a local change in the `SAFETY` optimization
policy.

### DECStation (PMAX) specific changes:

Representation conversion of a `SAP` (system area pointer) to a pointer
representation now results in an efficiency note.

Fixed EQL (and =) on integers to not unnecessarily cons a word-integer
argument just because one argument is known to be a fixnum.

New version of the assembler with instruction scheduling (no-op deletion)
support.  This reduced the size of the core by 1.3 meg, and makes
everything run faster too.

Fixed `TRUNCATE` on floats to truncate instead of rounding.

### SPARC notes:
The SPARC port is not yet as highly tuned as the PMAX port.  In particular,
no instruction scheduling is done yet.  This is probably a 10% performance
penalty.

### Enhancements:
Made forms within a `LOCALLY` be recognized as "top-level" so that subforms
can be compiled separately.

The compiler now ignores assignments to special variables when determining
which representation to use for a variable.  Also, we don't print
representation selection efficiency notes about coercion that are due to
error checking code.

Added support for the `EXT:CONSTANT-FUNCTION` declaration (already in the
documentation.)

When a `DEFUN` is compiled and the name has a `FTYPE` declaration, then a note
is printed if any arguments to the function are assigned to (i.e. `SETQ`) in
the body, as this inhibits application of the `FTYPE` declaration to the
argument variables.

(<mumble>-P x) structure predicates are now just as efficient as
(`TYPEP` x '<mumble>).

Added type inference methods for sequence functions, and various functions
that return an argument as their result value.

A number of improvements to register allocation.

Added a new optimization of `MULTIPLE-VALUE-CALL` which converts MV calls
having a known number of arguments into `MULTIPLE-VALUE-BIND`/`FUNCALL`.
Combined with some other existing optimizations, this allows functions like
to be efficiently inline expanded (i.e. the `APPLY` turns to a `FUNCALL`):
```
	(defun foo (&rest x)
	  (apply #'glorp x))
```

Reduced the size of debug information for `OPTIMIZE` `DEBUG` <= to 1.
If debug-info is < 1, then don't dump debug-args or function type.

Disabled the compiler's internal consistency checking by default.  These
phases are only useful for locating compiler bugs.

### X3J13 cleanups:
The `:VERBOSE` and `:PRINT` keyword arguments are now supported by
`COMPILE-FILE`.  The `:PROGRESS` keyword is a CMU extension that provides an
even higher level of verbosity.  The `*COMPILE-VERBOSE*`, etc., variables are
also now supported.

Changed declaration processing to treat `FUNCTION` declarations as ordinary
variably type declarations.  The old semantics is still obtained when the
second arg to the declaration is a list (as it always would be in the old
usage.)

### Block compilation:
Added new `START-BLOCK` and `END-BLOCK` declarations to allow portions of a
file to be block compiled (instead of only entire files.)  This mechanism
also allows the entry points to a block to be specified, allowing improved
compilation of non-entry-point functions.  Fixed many bugs that appeared
once block compilation was actually used.

`COMPILE-FILE` now has `:ENTRY-POINTS` and `:BLOCK-COMPILE` keywords.
`:BLOCK-COMPILE` `NIL` will totally inhibit compile-time resolution of function
names (including self-calls.)  The default (`:SPECIFIED`) allows compile time
resolution, but still compiles one top-level form at a time, preventing
local calls between top-level forms.  In this mode, a
```
        (BLOCK-START Entry-Point*)
```
declaration will start block compilation.  Block compilation is terminated
by `BLOCK-END`, or the `BLOCK-START` of the next block.

See also the `COMPILE-FILE` doc string.


### Context sensitive declarations:
Added the `OPTIMIZE-INTERFACE` declaration, which is just like `OPTIMIZE`, but
specifies the policy for function argument syntax checking and checking of
any declared argument types, allowing it to be distinct from the general
compilation policy.  This allows debugged code to be compiled with lowered
safety in its "guts", while still doing checking on the arguments that
users may supply (incorrectly.)  Any quality not separately specified
defaults to the normal `OPTIMIZE` quality.

Fixed `WITH-COMPILATION-UNIT` keyword to be `:OVERRIDE` instead of `:FORCE`.
Also, added `:OPTIMIZE` and `:OPTIMIZE-INTERFACE` for changing the "global"
compilation policy within the dynamic extent.

Added `:CONTEXT-DECLARATIONS`, which provides a way to insert declarations
conditional on pattern matching of the context in which the definition
appears.  So you can compile all external functions safe, or whatever.  See
the doc string for `WITH-COMPILATION-UNIT`.


## Hemlock:

### Tuning:
Changed typescript streams to cache the line length.  This greatly speeds
up slave output.

Several changes to allow redisplay to be delayed until process output (i.e.
in a shell buffer) is complete.  This allows the editor to catch up with
output by only displaying the final state of the shell buffer, instead of
forcing every line of output to be displayed.  This is very nice with slow
terminals or large outputs.

### TTY redisplay:
Changed TTY redisplay to get the terminal size and speed from Unix using
the appropriate "ioctl" calls.  The speed of a PTY (and hence any telnet or
MCN connection) is infinite by default.  For best results with TTY
redisplay, it is crucial to set the terminal speed with the Unix "stty"
command:
```
    	stty 2400
    	stty 9600 etc.
```

Setting the speed allows the editor to keep in synch with the terminal so
that typing a command will temporarily abort redisplay until until there is
no typeahead.  This way, if you type C-v C-v in succession, output of the
first screen will stop when you type the second C-v.

Fixed several bugs in TTY redisplay.  "Unexpected zero transition delta" is
gone.  Also, fixed some problems with the screen not being updated properly
after redisplay has been aborted.  (When you type several commands in quick
succession.)

`REDISPLAY` now returns T, `NIL` or `:EDITOR-INPUT`.  T is returned when
redisplay changed the screen.  `NIL` is returned when there was no change.
`:EDITOR-INPUT` is returned when redisplay tried to update the screen, but
was aborted due to pending editor input.

Fixed `REDISPLAY-WINDOWS-FROM-MARK` so that process output won't cause
redisplay when we aren't in Hemlock.

### Bug fixes:
Modified `MAKE-BUFFERS-FOR-TYPESCRIPT` to make sure the user supplied
slave-name is free for use, so we don't clobber currently existing slaves.

Fixed a bug in completion mode (didn't previously work in the new-compiler
system.)

### Enhancements:
There is a new command "Set Buffer Writable", and the obsolete
command "Connect to Registered Eval Server" has been removed.

Added "Slave GC Alarm" variable (default `:MESSAGE`) which controls how
obnoxious the slave GC notification is.  Other values are like for "Input
Wait Alarm", `:LOUD-MESSAGE` and `NIL`.

Made the slave switch demon set `debug:*help-line-scroll-count*` to
`most-positive-fixnum`, since the editor can do the scrolling for us.


## SYSTEM, EXTENSIONS:

Made `SYSTEM:BITS`, `BYTES`, etc., be defined in the null environment so that they
can be inline expanded.  This was causing spurious consing in various system
code.

Fixed `EXT:CONNECT-TO-INET-SOCKET` to check that we successfully looked up the
name so that we don't get segment violations on unknown hosts.

Fixed `DI:FUNCTION-DEBUG-FUNCTION` (though it still returns the XEP.)  
Some fixes to DI: condition report methods

Added support for the `MACH:TIOCGWINSZ` and `MACH:TIOCSWINSZ` ioctls.
In the Unix interface, extended the length of pathnames from 64 to 1024.

`EXT:ONCE-ONLY` now does sequential variable binding.  This can't cause any
problems, since all names are gensyms, and is often useful.

Added `:TIMEOUT` argument to `SYSTEM:MAKE-FD-STREAM`.  The `SYSTEM:IO-TIMEOUT`
condition is signalled if a timeout is specified and exceeded.

