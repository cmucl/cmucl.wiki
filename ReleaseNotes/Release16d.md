# Release notes for CMU Common Lisp 16d, 30 May 92

16d is our first version 16 general release, and incorporates many changes not
present in the 15 series.  It is currently fairly close to our current
internal development (alpha) systems, and is thus less stable.  The major
changes are:
  *  New Aliens
  *  New pathnames
  *  New pretty printer
  *  New format
  *  R5.0 CLX.  
  *  5/1/90  May Day PCL (REV 4b)
  *  Revised manual

The fasl file format is nominally compatible with version 15, but the pathname
change affects any pathname constants in fasl files, which includes the
defined-from information present in every fasl file.  It is probably a good
idea to recompile everything.

CLX and Hemlock are now optional.  When CMU CL is installed, the maintainer can
elect not to load CLX and Hemlock -- this saves 7 megabytes of disk and
improves memory usage somewhat.  See the installation section of the README
file for details.

The ``CMU Common Lisp User's Manual'' has been updated to be more helpful for
non-CMU users.  The new manual also documents the new Alien facility for
foreign function calls and data structure access.  The manual is now formatted
with Mike Clarkson's LaTeXinfo package, so a consistent version of the
documentation is available online in Gnu info format.  See `doc/cmu-user.ps'
and `doc/cmu-user.info'.
 

## General system code:

### ANSI cleanups:
 - ANSI Compiler macros are now implemented: see `COMPILER-MACRO-FUNCTION`,
    `COMPILER-MACROEXPAND`, `COMPILER-MACROEXPAND-`1 and `DEFINE-COMPILER-MACRO`.
 - Fixed things that invoke `*MACROEXPAND-HOOK*` to coerce it to a function
    before calling it.
 - Fixed `MACRO-FUNCTION` to take an environment argument.
 - `SYMBOL-MACROLET` now accepts declarations.
 - `SHADOW` now accepts strings in addition to symbols.
 - Added `UPGRADED-ARRAY-ELEMENT-TYPE` and `UPGRADED-COMPLEX-PART-TYPE`.
 - `IGNORABLE` is now in the `LISP` package instead of the `EXT` package.
 - `ADJUST-ARRAY` has been updated to allow adjusting of arrays which were
    not created with :adjustable non-nil to be adjusted to new dimensions.
 - `ADJUSTABLE-ARRAY-P` has been updated correspondingly. It returns T if
    adjust `ADJUST-ARRAY` would return an `EQ` array.
 - The `BASE-CHARACTER` type has been renamed to `BASE-CHAR`.
 - The `REAL` type and `REALP` function are now implemented.
 - Changed the default structure printer to print slot names as keywords
    instead of unqualified symbols.

### Enhancements:
 - Added `MAYBE-INLINE` declaration for `GET`, `PUT`, etc., so that these
    functions can be inline expanded according to the compilation policy.
 - Added some type declarations so that `GET-INTERNAL-REAL-TIME` doesn't cons.
 - Process the command line before printing the herald so that we can eval
    some form and quit without printing anything.
 - `SET` now protects against setting `T`, `NIL`, and keywords.  `(SETF
    SYMBOL-FUNCTION)` now expands into `FSET`, which protects against defining
    `NIL`.
 - Substantially rearranged function describing to make it more consistent,
    and added support for describing interpreted functions.
 - `PURIFY` is now called multiple times during system building to improve
    locality.  
 - `(EVAL-WHEN (EVAL) ...)` is now actually eval'ed.

### Bug fixes:
 - Fixed bug in `NTH-VALUE` where it expanded into bogus code unless ``n'' was
    a constant integer.
 - Fixed `FMAKUNBOUND` to return the symbol instead of T.
 - Allocate memory as executable so that the OS knows to maintain cache
    consistency.
 - Changed `DESCRIBE` to allow `T` or `NIL` as the stream argument.

### Load enhancements and cleanups:
 - The initial value of `*LOAD-VERBOSE*` is now T.  Additionally, `LOAD` no
    longer always binds `*LOAD-VERBOSE*` and `*LOAD-PRINT*`.  Now it only
    binds them when `:VERBOSE` or `:PRINT` are explicity supplied.  Therefore, you
    can set either of these in your init file and it will take effect.
 - Normally when `*LOAD-VERBOSE*` is T, only the file name is printed.
    Formerly, the loaded stream was always printed, whereas now a stream is
    printed only when the stream is not a file stream.
 - Added ANSI features `*LOAD-TRUENAME*`, `*LOAD-PATHNAME*` and `*LOAD-PRINT*`.
 - As per ANSI, bind `*READTABLE*` to itself to make assignments file-local.
 - Added new variables `EXT:*SOURCE-FILE-TYPES*` and `EXT:*OBJECT-FILE-TYPES*`.
    When no file type is specified, `LOAD` tries the types in these lists to
    locate the source and object files.  `LOAD` now recognizes source types "l",
    "cl" and "lsp" in addition to "lisp".
 - The compiler `OPTIMIZE` policy is now bound during load, so proclamations in
    a file don't leave the global policy clobbered when the load is finished.
 - Changed the `:IF-SOURCE-NEWER` option to signal an error and use restarts,
    rather than `PROMPT-FOR-Y-OR-N`.  Fixed the load source case to actually
    load the source, rather than loading the object as a source file...
 - Changed load to deal with source files having `NIL` type more reasonably.
 - If a wild pathname is given to `LOAD`, all files matched will be loaded.
 - Proceeding from nonexistent file errors has been improved.  It is no longer
    assumed that missing files are always source files.  Added condition
    restarts for missing files.
 - Improved formatting of error and warning messages.


### Garbage collection:
 - Changed the minimal `ROOM` output to include all easily computed information
    including whether GC is disabled.  The verbose `ROOM` now conses less.
 - Removed the `:ENABLE-GC` `SAVE-LISP` option, as it's no longer needed.
    Garbage collection is now correctly enabled in cores which have been saved
    and then restarted.
 - Added `EXT:BYTES-CONSED-BETWEEN-GCS`, a function that returns (and sets when
    used with setf) `*BYTES-CONSED-BETWEEN-GCS*`.  Additionally, it changes
    `*GC-TRIGGER*` immediately to reflect the new values of *bytes-consed...*.
 - `TIME` now displays the `GC` run-time.
 - Added `EXT:*GC-RUN-TIME*` with accumulates the `INTERNAL-RUN-TIME` spent doing
    garbage collection.  Added declarations to make `EXT:GET-BYTES-CONSED` more
    efficient.
 - The top-level REP loop now zeros the unused non-zero portion of the
    control stack to discourage spurious garbage retention.
 - The garbage collector now closes open file streams when it reclaims them.


### Packages:
 - The `LISP` and `USER` packages have been renamed to `COMMON-LISP` and
    `COMMON-LISP-USER`.  `LISP` and `USER` are nicknames, so they can still be used.
 - The `LISP` package namespace has been cleaned up somewhat.  For example,
    `*DESCRIBE-PRINT-LEVEL*` is no longer exported from `LISP`.
 - The Mach/Unix division in the package system has been clarified a great
    deal.  Unix specific features have been moved from the MACH package to the
    UNIX package.  Mach specific features have been left in (or moved to) the
    MACH package.  For example, all standard Unix syscalls are related
    definitions are un UNIX, whereas vm_statistics remains in MACH and GR-CALL
    has been moved to MACH.

### SETF cleanups:
 - Changed `GET-SETF-METHOD-MULTIPLE-VALUE` to try to macroexpand-1 the form
    when it's an atom in case it's a symbol-macro as per the X3J13 cleanup
    `SYMBOL-MACROLET-SEMANTICS:SPECIAL-FORM`.  Now you can safely `INCF`, etc.
    symbol macros where the macroexpansion has side effects.
 - Fixed `SETF` of `GETF` to evaluate the various parts in the correct order as
    per X3J13 cleanup `SETF-SUB-METHODS:DELAYED-ACCESS-STORES`.
 - X3J13 cleanup `SETF-MULTIPLE-STORE-VARIABLES:`
    Extend the semantics of the macros `SETF`, `PSETF`, `SHIFTF`, `ROTATEF`, and
    `ASSERT` to allow "places" whose `SETF` methods have more than one "store
    variable".  In such cases, the macros accept as many values from the
    newvalue form as there are store variables.  As usual, extra values
    are ignored and missing values default to `NIL`.
 - Extended the long form of `DEFSETF` to allow the specification of more
    than one "store variable", with the obvious semantics.
 - `GET-SETF-METHOD` signals an error if there would be more than one
    store-variable. 


## Printer:

Almost all of the printing code has been rewritten/restructured to support
all of the printing features added to the language.  Some highlights:
 - `*PRINT-READABLY*` is now supported.
 - `*PRINT-CIRCLE*` works irrespective of `*PRINT-PRETTY*`.  Note: the default
    structure printer currently does not work when `*PRINT-CIRCLE*` is true: you
    get `#1=#1#`.
 - `*PRINT-LEVEL*` abbreviation now works automatically inside structure
    printers.
 - XP has been replaced with a native pretty printer that is fully
    integrated with the rest of the system.  This Supports the ANSI
    pretty-printing interface instead of the old Waters XP interface.  Existing
    uses of the old interface will need to be updated to use the new names.
 - The pretty-printer now unparses backquote forms on printing.  To retain
    this information, the backqoute read macro no longer expands into `LIST`,
    `CONS`, etc.  Internal functions are used instead.
 - The macros `WITH-STANDARD-IO-SYNTAX` and `PRINT-UNREADABLE-OBJECT` have
    been added.
 - All new format.  Supports the `FORMATTER` macro and all the pretty-printing
    directives.  `FORMAT` has extended to accept a function as the format control
    (as an alternative to a string.)
 - Added support for `READTABLE-CASE` in symbol printing.  Printing when
    `*PRINT-CASE*` is `:CAPITALIZE` and `*PRINT-ESCAPE*` is `NIL` is now slightly
    different than before.  Added some missing array type declarations in
    symbol printing.

### Bug fixes:
 - Fixed a bug which caused some float printing format directives to
    infinitely loop when a fixed-width field overflowed.
 - Specify stream when printing unbound marker.
 - Fixed `FORMAT` to override printer control variables when printing float
    exponents so that they are always printed in decimal, etc.


## Reader:

### ANSI Cleanups:
 - `*READ-EVAL*` is now supported.  If a #. is encountered while `*READ-EVAL*`
    is `NIL` (default T), an error is signaled.  This intended to allow
    ``secure'' `READ`-based command interfaces to be written.
 - `READTABLE-CASE` is now supported.
 - The reader now signals the correct type of error when things go wrong
    instead of always signaling a simple-error.
 - Changed the return value of `SET-SYNTAX-FROM-CHAR` from `NIL` to T as per X3J13
    cleanup `RETURN-VALUES-UNSPECIFIED:SPECIFY`.  [Hard to believe nobody has
    complained about not conforming to this one.]

### Bug fixes:
 - Fixes to several bugs with respect to #+, #-.  In particular, stacked
    conditionals like "#+foo #-bar baz" now work 
 - #n= and #n# now detect more error conditions and work on structures.
 - # is now a non-terminating macro character, so foo#bar is read as a
    symbol.
 - Added Ted's changes to make `INTERNAL-READ-EXTENDED-TOKEN` work when there
    are `|' escapes.  The main significance of this is that #+nil '|foo;bar|
    and #:|foobar| now work properly.  Also changed this function to recognize
    unquoted colons so that #:foo:bar will error, but not #:foo\:bar.

### Enhancement:
 - Export new variable *ignore-extra-close-parentheses* if true (the default),
    extra close parens are only a warning, not an error.  Previously unmatched
    close parens were quietly ignored.


## Pathnames:

This release supports all the new CltL2 pathname features except for logical
pathnames.  Following is an overview of the new pathname support.

Programs that actually conform to the CLtL1 pathname spec will have very few
problems.  However, the CLtL1 spec was extremely vague, and CMU CL did not
make use of much of the allowed flexibility, so many technically non-portable
programs previously worked under CMU CL.

The main incompatible changes from CLtL1 to CLtL2:
 - Symbols are no longer acceptable filenames.
 - `PATHNAME-HOST` may be any object.
 - `:UNSPECIFIC` is now a legal pathname component.
 - `MERGE-PATHNAMES` now recognizes relative pathnames and treats them
    specially. 

The format of directories is now specified (to be a list in a certain format.)
This required an incompatible change from the previous practice of using a
vector `PATHNAME-DIRECTORY` and using "DEFAULT" or `:ABSOLUTE` in the
`PATHNAME-DEVICE` to indicate relative and absolute pathnames.

In a related change, the CMU `SEARCH-LIST` extension was changed so that the
search-list now appears in the `PATHNAME-DIRECTORY` as:
```
    (:ABSOLUTE #<Search-list "name"> ...)
```
Other changes to search-lists:
 - `(SETF SEARCH-LIST)` now accepts a string or pathname, and converts it into
    a one-element list.
 - Search-list elements are now canonicalized to pathnames rather than to
    strings. 
 - Instead of returning `NIL`, `SEARCH-LIST` now signals an error when it is
    called on an undefined search list. 
 - `SEARCH-LIST-DEFINED-P` is a predicate that tells if the search list is
    currently defined.

New features which are now supported:
 - Wildcard pathnames are now fully supported.  In addition to allowing `:WILD`
    in any pathname component, "extended wildcards" such as "foo*.*" are also
    supported.  A consequence of this is that `PATTERN` objects may appear in
    wildcard pathname components instead of strings.  See `PATHNAME-MATCH-P` and
    `TRANSLATE-PATHNAME`.
 - As a CMU CL extension, a wildcard pathname may be used as the argument to
    any filesystem interface (like `OPEN`) as long as it matches only one file.
 - The pathname `:COMMON` case mechanism provides a way around the problems of
    portably specifying string pathname components in the presence of operating
    systems with differing preferred case in their filesystem.  An uppercase
    string "LISP" is mapped to the "customary case" (lowercase on unix.)  
    Lowercase is also inverted: "readme" becomes "README".  Mixed case is left
    alone.  Note that this mechanism is explicitly enabled by supplying `:CASE`
    `:COMMON` to functions such as `MAKE-PATHNAME`.  The default is the old
    behavior `(:CASE :LOCAL)`.

Also, `DIRECTORY` now actually returns the `TRUENAME` of each file (as it was
always supposed to do.)  If a matched file is a symbolic link, the truename may
be a file in some other directory that doesn't even match the pattern.  The old
behavior can be obtained by specifying `:FOLLOW-LINKS` `NIL`.

The new wildcard pathname mechanism has not yet been used to replace the old
single-wildcard matching in Hemlock DIRED, etc.


Debugger:
 - Added Miles' changes to keep errors and warnings on one line if they fit.
 - The debugger now starts up with the error frame as the current frame, so
    it is no longer necessary to manually skip over internal frames resulting
    from the error system invocation.
 - Fixed some debugger bugs that appeared when debugging interpreted code.
 - Added ``DESCRIBE'' debugger command.
 - Merged Miles' changes that allow the use of restart names as debugger
    commands.
 - Changed `SHOW-RESTARTS` to also display the restart name (but only if it's
    not shadowed by a higher priority restart).  Changed the restart command
    to look for restarts by name if a symbol is typed.
 - Bind `*CURRENT-LEVEL*` to 0, `*PRINT-READABLY*` to nil, and `*READ-EVAL*` to T
    when entering the debugger to make sure things print as expected.

The debugger programmer (`DEBUG-INTERNALS`) interface is now documented in the
User's Manual.  This interface allows the coding of debuggers and debugger
extensions without requiring an intimate understanding of the compiler and
run-time system.  Be warned that `DI` features (such as breakpoints) not used by
the current debugger may not work very well (wait for version 17.)

Debug internals changes:
 - `DI:DEBUG-FUNCTION-FUNCTION` is now implemented.
 - Added `DI:FLUSH-FRAMES-ABOVE` for cleaning up frames to be bound to
    `DEBUG:*STACK-TOP-HINT*`.


## Defstruct:

Various fixes and enhancements to defstruct slot parsing and inclusion.   
It now works to define structures such as:
    (defstruct super a)
    (defstruct (sub1 (:conc-name super)) one)
    (defstruct (sub2 (:conc-name super)) two)
    (super-a (make-sub1))

previously, a definition such as for SUB2 would define SUPER-A to be a
SUB2-specific function, which could then no longer be used on other types.  The
spec doesn't clearly say that such a construct is legal, but its use seems
fairly common.

Also, slot parsing is now more rigorous.  Unrecognized slot options cause an
error, as does a slot spec like: (defstruct foo (a :type t))

Fasl dumping of constant structures has been fixed to conform to X3J13.  The
main significance of this is that `DEFSTRUCT` structures are no longer dumpable
by default.  However, the generic function `MAKE-LOAD-FORM` isn't really used.
Instead, a new defstruct option, `:MAKE-LOAD-FORM-FUN`, has been added that can
be used to specify a function that acts like a `MAKE-LOAD-FORM` method.  When we
have a `CLOS` that supports `STRUCTURE-CLASS`, the default method for
`MAKE-LOAD-FORM` will use this information instead of having the compiler use it
directly.  The old behavior can be enabled on a structure by structure basis by
using the `:MAKE-LOAD-FORM-FUN` defstruct option:
```
    (defstruct (foo (:make-load-form-fun :just-dump-it-normally))
      ...)
```

## Compiler:

### Cleanups:
 - Added the `LOAD-TIME-VALUE` support special form.
 - Displaced or adjustable arrays and vectors with fill pointers can now be
    dumped in fasl files, but are converted to simple array with the same
    elements.
 - Arrays of floats are left as arrays of floats instead of being
    converted into arrays of element-type T that just so happen to hold a
    bunch of floats.
 - Changed `IGNORE` and `IGNORABLE` to recognize #'fn-name for declaring that
    local functions are not used.  Exported `IGNORABLE` from `LISP`.

### New optimizations:
 - Iterations written using tail recursion are now optimized through a
    special-casing of local functions where all calls but one are
    tail-recursive self-calls.  Such functions are compiled with no
    environment manipulation, resulting in the same code as explicit
    iteration.
 - Loop rotation (Knuth "while" loop optimization) been added.  This is the
    optimization that negates the loop exit test and places it at the end of
    the loop, and then jumps there at loop entry.  Note that this is part of
    control optimization, and not simply a recoding of certain iteration
    macros.  In fact, for historical reasons `DO`, etc. already had the exit
    test negated, but the compiler was cleverly un-negating the test.
 - Flow analysis now recognizes function calls and special forms which do
    not yield any value because they unwind or signal an error.  This results
    in improved code for error checks, since the compiler can get by with
    fewer unconditional branches.  A function can be declared not to return by
    declaring its result type to be `NIL` (not to be confused with `NULL`).  If a
    function declared `NIL` does return, an error will be signalled.
 - Added derive-type methods for `ASIN`, `ACOS`, `ACOSH`, `ATANH` and `SQRT` which
    figure out whether the result type is real on the basis of the argument
    range.  Also, the result of `ATAN` is always real, so we don't need a result
    type assertion.
 - Added optimization which deletes `MULTIPLE-VALUE-BIND`s when all
    variables have been deleted.
 - Eliminated some spurious checking of the result types of safe `VOP`s (such
    as the SPARC tadd for fixnum arithmetic.)
 - Transform uses of the `SEARCH` generic sequence function on simple strings
    into a call to a more efficient string-specific function.
 - Added multiplier recoding of `(UNSIGNED-BYTE 32)` multiplication.  This
    converts 32 bit (or smaller) unsigned multiplication by any compile-time
    constant into a shift-add sequence.  This is much faster when the constant
    is a near power of two or when general multiplication is slow (as on the
    SPARC.)
 - Added comprehensive handling of arithmetic and logical identities when
    an arg is -1, 0 or +1.
 - Added a `RANDOM` derive-type method: (random 13) is (integer 0 12).


### Enhancements:
 - Changed the compiler to temporarily increase
   `*BYTES-CONSED-BETWEEN-GCS*`
     by a factor of 4 during compilation of each top-level form,
    instead of turning off all garbage collection.
 - Bind `*PRINT-LINES*` around compiler error output to
    `*ERROR-PRINT-LINES*`.
 - Do not warn about undefined variables, functions or types when the
    `INHIBIT-WARNINGS` `OPTIMIZE` quality is 3.
 - Some optimizations are now considered "important"; failure of important
    optimizations causes an efficiency note even when speed=inhibit-warnings
    (i.e. by default.)
 - Print a error summary even when `*COMPILE-VERBOSE*` is false.  (This is only
    printed when there are errors, so this doesn't seem a violation of the
    spirit of the spec.)

### Bug fixes:
 - Merged Miles' fix to disassembly of the MIPS coprocessor move instructions.
 - Fixed spelling of "efficency" in exported variables such as
    `*EFFICIENCY-NOTE-COST-THRESHOLD*`
 - Fixed some cases where incomplete optimization could happen.
 - Fixed some arithmetic "identities" that failed to preserve the sign of
    -0.0.
 - Fixed `TYPES-INTERSECT` to consider any supertype of T to definitely
    intersect with anything (even an unknown type.)
 - Fixed a problem where inconsistent declarations could fail to be detected
    at compile time.
 - Fixed the `VALUES` transform (which discards unused subforms) to work
    on `(VALUES)`.
 - More bug fixes to dead code deletion.
 - Fixed a problem where a special binding would not be removed on exit from
    the scope if there was no code in the scope (e.g. it had all been
    deleted.)
 - Fixed handling of named (`DEFCONSTANT`) constants so that `EQ`
    relationships are properly preserved.


## Extensions:

 - Export `FEATUREP` from `EXT`.  This takes a feature expression and tests it
    against the value of `*FEATURES*`.  Allow `LISP:AND`, `LISP:OR`, and `LISP:NOT` in
    features lists in addition to `:AND`, `:OR`, and `:NOT`.  This makes featurep
    useful outside of #+ and #-.
 - The encapsulation mechanism (similar to advise facilities) has been
    revamped to work on `SETF` function names as well as symbols.
    `EXT:ENCAPSULATED-DEFINITION`
       Returns whatever definition is stored for name, regardless of whether
       it is encapsulated.  Unlike `FDEFINITION`, this does not strip off the
       encapsulation.  This is `SETF`'able.
    `EXT:ENCAPSULATE`
       Replaces the definition of name with a function that binds name's
       arguments a variable named argument-list, binds name's definition to a
       variable named basic-definition, and `EVAL`'s body in that context.  Type
       is whatever you would like to associate with this encapsulation for
       identification in case you need multiple encapsuations of the same
       name.
    `EXT:UNENCAPSULATE`
       Removes name's most recent encapsulation of the specified type.
    `EXT:ENCAPSULATED-P`
       Returns t if name has an encapsulation of the given type, otherwise
       nil.
    `EXT:*SETF-FDEFINITION-HOOK*`
       A list of functions invoked by (`SETF` `FDEFINITION`) before storing the
       new value.  Each hook function must take the function name and the
       new-value.

### Hemlock:
 - Fixed the `:FILE` branch of "Help on Parse" to trim leading directory
    components off the pathname if it wouldn't otherwise fit on the screen.
 - Fixed `GET-EDITOR-TTY-INPUT` to not assume that `UNIX-READ` will always work.
 - When we reallocate string table vectors to grow them, clear the old vector
    so that it won't hold onto garbage (in case the vector was in static space,
    but pointed to dynamic values.)  This was a major cause of memory leakage
    in Hemlock.
 - Added a (setf (ts-stream-char-pos stream) 0) to the accept-input function
    so that char-pos will be reset to zero whenever the user presses enter.

### PCL:
 - The version has been updated to "5/1/90  May Day PCL (REV 4b)".
 - The Code walker now understands the real `SYMBOL-MACROLET`, and the PCL macro
    definition is no longer used.
 - Fixed a bug in `WALK-ARGLIST` where it would ignore the rest of the current
    arglist if the current arg destructured.  This was causing it to compile
    forms like:
	    (macrolet ((foo ((a b) c) ...)) ...)
    as:
	    (macrolet ((foo ((a b)) ...)) ...)
    note the loss of the arg c.


## System:

 - All the `SAP-REF-<n>` functions now take byte offsets.  Previously, the
    16 and 32bit versions were scaled by the element size.
 - Fixed `UNIX-IOCTL` to not flame out of the cmd is a ub-32 instead of a sb-32.
 - Added Miles' `TCSETPGRP`, `TCGETPGRP`, and `TTY-PROCESS-GROUP`.
 - Unix syscalls are now more restrictive in the kind of arguments that they
    accept.  In particular, `UNIX:UNIX-READ`, etc., no longer automatically
    accept a vector (or string) argument.  You must use `VECTOR-SAP` to convert
    the vector to a system area pointer.  Note that `WITHOUT-GCING` should be
    wrapped around any syscall which is passed a pointer to the Lisp heap,
    since the object might otherwise move doing the syscall.
 - Changed `LOAD-FOREIGN` to be exported from `ALIEN`.  Changed it have keyword
    args instead of optionals.  Deleted obsolete linker argument.
