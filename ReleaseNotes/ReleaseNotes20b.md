<!-- Name: ReleaseNotes20b -->
<!-- Version: 2 -->
<!-- Last-Modified: 2012/01/07 15:53:28 -->
<!-- Author: rtoy -->
# Release Notes for CMUCL 20b

The CMUCL project is pleased to announce the release of CMUCL 20b.
This is a major release which contains numerous enhancements and
bug fixes from the 20a release.

CMUCL is a free, high performance implementation of the Common Lisp
programming language which runs on most major Unix platforms. It
mainly conforms to the ANSI Common Lisp standard. CMUCL provides a
sophisticated native code compiler; a powerful foreign function
interface; an implementation of CLOS, the Common Lisp Object System,
which includes multi-methods and a meta-object protocol; a source-level
debugger and code profiler; and an Emacs-like editor implemented in
Common Lisp. CMUCL is maintained by a team of volunteers collaborating
over the Internet, and is mostly in the public domain.

New in this release:

  * Known issues:

  * Feature enhancements:
	can be given.  If a character, then that character is used as
	the replacement character.  For a symbol or function, it must be
	a function of 3 arguments:  a message string, the offending
	octet (or nil), and the number of octets read in the encoding.
	If the function returns, it must be the codepoint of the desired
	replacement.
	given.  If a character, then that character is used as the
	replacement character.  For a symbol or function, it must be a
	function of 2 arguments: a message string and the offending
	codepoint.  If the function returns, it must be the codepoint of
	the desired replacement.

  * ANSI compliance fixes:

  * Bugfixes:

  * Trac Tickets:

  * Other changes:

  * Improvements to the PCL implementation of CLOS:

  * Changes to building procedure:

This release is not binary compatible with code compiled using CMUCL
20a; you will need to recompile FASL files. 

See <URL:http://www.cons.org/cmucl/> for download information,
guidelines on reporting bugs, and mailing list details.


We hope you enjoy using this release of CMUCL!

