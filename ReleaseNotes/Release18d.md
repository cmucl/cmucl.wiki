<!-- Name: Release18d -->
<!-- Version: 1 -->
<!-- Last-Modified: 2014/02/07 10:17:52 -->
<!-- Author: rtoy -->
### ======================= C M U C L  18 d ==========================

The CMUCL project is pleased to announce the release of CMUCL 18d.
This is a major release which contains numerous enhancements and
bugfixes from the 18d release.

CMUCL is a free, high performance implementation of the Common Lisp
programming language which runs on most major Unix platforms. It
mainly conforms to the ANSI Common Lisp standard. CMUCL provides a
sophisticated native code compiler; a powerful foreign function
interface; an implementation of CLOS; the Common Lisp Object System;
which includes multimethods and a metaobject protocol; a source-level
debugger and code profiler; and an Emacs-like editor implemented in
Common Lisp. CMUCL is maintained by a team of volunteers collaborating
over the Internet, and is in the public domain.

Features new in this release:
  * Feature enhancements:

  * Numerous ANSI compliance fixes:
  * Numerous bugfixes:

  * Other changes:

  * Code cleanups:

   * Changes to rebuilding procedure:

This release is not binary compatible with code compiled using CMUCL
18c; you will need to recompile FASL files. 

See <URL:http://www.cons.org/cmucl/> for download information,
guidelines on reporting bugs, and mailing list details.


We hope you enjoy using this release of CMUCL!
