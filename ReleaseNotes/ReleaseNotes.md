<!-- Name: ReleaseNotes -->
<!-- Version: 23 -->
<!-- Last-Modified: 2014/10/14 22:31:12 -->
<!-- Author: rtoy -->
# Release Notes

Here are the release notes for various selected versions of CMUCL.

 * [Release 20f](ReleaseNotes/Release20f), 2014/10/04
   * Support for x87 dropped
   * fdlibm
 * [Release 20e](ReleaseNotes/Release20e), 2013/09/28
   * First release supporting only unicode
 * [Release 20d](ReleaseNotes/Release20d), 2012/10/27
   * Last release with support for 8-bit chars
 * [Release 20c](ReleaseNotes/Release20c), 2011/11/02
 * [Release 20b](ReleaseNotes/Release20b), 2010/09/27
   * Static (non-movable) arrays
 * [Release 20a](ReleaseNotes/Release20a), 2009/10/28, 
   * Unicode support
 * [Release 19f](ReleaseNotes/Release19f), 2009/03/11, 
   * SSE2 support
 * [Release 19e](ReleaseNotes/Release19e), 2008/04/30
   * Mac OS X on Intel
 * [Release 19d](ReleaseNotes/Release19d), 2006/11/14, 
   * `DOUBLE-DOUBLE` support
   * Hash tables supporting weak value, weak key-and-value, and weak key-or-value.
 * [Release 19c](ReleaseNotes/Release19c), 2005/11/14
   * XREF support via compile-file :xref
 * [Release 19b](ReleaseNotes/Release19b), 2005/06/26
   * Mac OS X on PPC
   * NetBSD on x86
 * [Release 19a](ReleaseNotes/Release19a), 2004/07/27
   * Package locks
   * Hash tables with weak keys
   * Callbacks from foreign code to lisp (x86 and sparc)
   * Modular arithmetic (x86 and  sparc)
   * Gencgc for sparc.
 * Release 18f: There was no 18f release, skipping immediately to 19a.
 * [Release 18e](ReleaseNotes/Release18e), 2003/04/13
   * Karatsuba multiplier
   * Linkage tables
   * Cross-reference
   * Last release for Alpha on Linux
   * Last release for x86 on OpenBSD.
   * Last release for MIPS on SGI/Irix..
 * [Release 18d](ReleaseNotes/Release18d), 2002/04/09
   * OpenBSD/x86
 * [Release 18c](ReleaseNotes/Release18c), 2000/11/27
   * Support for `(complex single-float)` and `(complex double-float)`
 * Release 18b, 1998/07/20
   * Last release for Alpha on Digital Unix.
 * Release 18a, 1997/09/11
   * Last release for PA-RISC on HP/UX.
   * Last release for Sparc on SunOS..

Older releases, before open era of CMUCL
 * [Release 17f](ReleaseNotes/Release17f), 1994/11
 * [Release 17e](ReleaseNotes/Release17e), 1994/03/11
 * [Release 17c](ReleaseNotes/Release17c), 1993/09/19
 * [Release 16f](ReleaseNotes/Release16f), 1992/12/11
 * [Release 16e](ReleaseNotes/Release16e), 1992/08/05
 * [Release 16d](ReleaseNotes/Release16d), 1992/05/30
 * [Release 15e](ReleaseNotes/Release15e), 1992/02/25
 * [Release 15d](ReleaseNotes/Release15d), 1992/02/02
 * [Release 15b](ReleaseNotes/Release15b), 1991/10/19
 * [Release 14c](ReleaseNotes/Release14c), 1991/06/06

