<!-- Name: FeatureList -->
<!-- Version: 1 -->
<!-- Last-Modified: 2014/09/04 20:55:17 -->
<!-- Author: rtoy -->
Common Lisp implementations use the variable `*features*` to reify information on implementation- or environment-specific properties and functionalities. This page explains the meaning of the features provided by CMUCL.

# Standardized

:ieee-floating-point
:x3j13
:ansi-cl
:common-lisp

# Platform-related

:sparc
:sun4
:sparcstation
:sparc-v7
:sparc-v8
:sparc-v9
:x86
:hppa
:alpha
:sgi
:solaris
:sunos
:svr4
:linux
:glibc2
:bsd
:freebsd
:mach
:irix
:hpux
:unix

# Implementation-specific

:cgc
:gencgc
:mp
:small
:random-mt19937
:hash-new
:complex-fp-vops
:pcl
:gerds-pcl
:portable-commonloops
:pcl-structures
:new-assembler
:python
:cmu
:cmu17
:cmu18
:cmu19
:common
:clx
:clm
:hemlock
:modular-arith
:double-double
:stack-checking
:heap-overflow-check

# Deprecated features no longer used

Here is a list of some of the features that used to be available but are no longer available. Many of these features are now standard parts of the build.

:fast-maxmin
:propagate-fun-type
:propagate-float-type
:constrain-float-type
:new-compiler