# Information for Developers

Developers may be interested in these items.

## How to Build Cmucl
* [Getting Tool to Build Cmucl](Developer/BuildTools)
* [Building Cmucl](Developer/BuildingCmucl)
* [Building on FreeBSD](Developer/building-on-freebsd-(version-10-and-above))

## Working on Cmucl
* [How to Define New Instructions](Developer/Defining-Instructions)
* [Using the Assembler](Developer/How-to-Use-the-Assembler)
* [LispUnit Testing](Developer/LispUnit)
* Sparc 64 information 
  * [Cross compiling](Developer/sparc64xcompile)
  * [Notes on Building for Sparc 64](Developer/sparc64-notes)
* [How to Update Unicode](Developer/UpdatingUnicode)
* [Git and Cmucl](Developer/GitAndCmucl)
* [Git and Trac](Developer/GitAndTracIntegration); this information is obsolete
  since Cmucl doesn't use Trac anymore, but the Trac instance is still
  available.
  
## Release Engineering
* [Make a Snapshot Release](Developer/HowToMakeASnapshot)
* [Update Release Version](Developer/how-to-update-release-version)

## Continuous Integration
* [Setting up VirtualBox Runners](Developer/VirtualBox-Runners)

## Improvements
* [Enhancing GC](Developer/CMUCL-GC-Enhancements)

