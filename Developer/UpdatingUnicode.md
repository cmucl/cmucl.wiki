<!-- Name: UpdatingUnicode -->
<!-- Version: 3 -->
<!-- Last-Modified: 2014/03/15 10:21:27 -->
<!-- Author: rtoy -->
# How to Update Unicode Support

Here are the steps for updating the Unicode support in CMUCL. First,
find the desired version of Unicode from [Unicode.org](http://unicode.org).

 1. Update all the files in `src/i18n`. For example, for Unicode
 1. Modify `src/tools/build-unidata.lisp` and `src/code/unidata.lisp` to have the desired values for the Unicode major, minor, and update versions.
 1. Increment the revision number in `*unidata-version*`.
 1. Follow the instructions at the top of `src/tools/build-unidata.lisp`:
    1. Compile and load `src/tools/build-unidata.lisp`
    1. Run `(build-unidata)`.  Specify the directory (to `src/i18n/` if necessary.
    1. If there are significant changes, `build-unidata` may not work. You will have to update `build-unidata.lisp` to conform to the new information.
       1. Updates to `build-unidata` may require corresponding changes to `write-unidata`.
       1. Run `(write-unidata <path>)` to write out the new `unidata.bin` file to `<path>`.
      1. Copy the new `unidata.bin` file to `src/i18n/unidata.bin`.
 1. Rebuild cmucl
 1. Using the new version of cmucl
    1. Run the tests using either
       1. The full lisp-unit test-suite
       1. Use `lisp-unit` and load just `tests/unicode.lisp`. Then run the unicode tests.
       1. There should be no failures in the tests.  If there are, fix up errors and start again.

This should give a new version of cmucl with support for the new version of Unicode.
