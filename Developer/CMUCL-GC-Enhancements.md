From a discussion Carl and I had about GC in CMUCL.

## Speeding Up Scavenging

In lisp/gencgc.c in the function [`garbage_collect_generation`](https://gitlab.common-lisp.net/cmucl/cmucl/-/blame/master/src/lisp/gencgc.c#L7875) there is
this bit of code: 
```C
	/*
 	 * All generations but the generation being GCed need to be
 	 * scavenged. The new_space generation needs special handling as
 	 * objects may be moved in - it is handle separately below.
 	 */
	for (i = 0; i < NUM_GENERATIONS; i++)
          if (i != generation && i != new_space)
    	        scavenge_generation(i);
```

This does too much work because all pages get scavenged. Why? Pages
are write-protected when they are known to have no pointers to younger
generations and these write-protected pages can be skipped. That’s
good, but that doesn’t mean all other pages that aren’t
write-protected should be scavenged.  Why? Consider a page from
generation 5. It could have pointers to generation 2. Because 2 is
younger than 5, the page is not write-protected, so it would be
scavenged. But this isn’t needed. We only need to scavenge the page if
it has pointers to generation 0.

Carl: It might be just as efficient to make the "card table" dense and
fast to scan as it would be to make it sparse and more expensive to
scan. 

How can we fix this? Each entry in the page table already keeps track
of the generation of the page. What we need is an additional slot to
indicate the youngest generation of any pointer in the page. Then when
scavenging happens we can just check if the youngest generation
matches the generation we’re scavenging.  If there’s no match, it can
be skipped.

How can we do this? Carl said this can be done when the write barrier
is hit or when we do GC. Doing this during a GC makes the most sense
because we use the trick of not reprotecting a page after the first
protection violation occurs. So, when we scan that page for pointers,
we need to keep track of each generation we point into and set the
value for the current page to the minimum generation found.

Other thoughts: `unprotect_oldspace` removes write-protect from all
protected pages so the `writeprotect`/`writeprotectedcleared` flag may be
useless and can be removed.

## Scavenging the Static Space
Currently, the entire static space is always scavenged.  Luke Gorrie
long ago pointed out that in his app that the generational GC worked
really well, but that scavenging the static space represented a
significant fraction of the time in GC. I don’t remember exactly what
the fraction was, but it was probably half or more.

A write barrier can be added to the static space just like for the
heap.  The static space can be treated as if it were generation 7 (the
dynamic space has 6 generations by default). Then we can use the same
ideas used for the dynamic space to reduce the number of pages that
need to be scanned.

There are some fiddly details that need to be addressed like the fact
that the page table assumes all the pages are contiguous. The static
space isn’t contiguous with the dynamic space, but it could be
arranged that way. Or the page table addressing could be updated to
support this discontinuity. Either should work.

## Additional considerations
The speed of the scanning loop is critical.  If data is ordered in the
heap by generation, a simple greater than or less than check could
suffice as a fast generational check.  Something like this
```C
void *page = ...
int nwords = size_in_words(scanp++)
for (i = 0; i < nwords; ++i) 
  if (*scanp <= page) {
    /* fast path, just scan the object */
  } else {
    /* slow path, object is younger */
  }
``` 
Because the generation boundaries are dynamic, checking the
generation can cost a few loads and branches.  The filter should keep
the inner loop of the scanning routine tight.

If you are clever, you should be able to avoid these checks in the
scanning routine.  You can separate the scanning routine into two
versions.  One for visiting the remembered set (scanning the locations
in pages that might contain roots) and the other for scanning the
condemned generation (the pages you are actually collecting).  You can
go one step further and write a function for visiting roots that
simply takes a callback.  This would unify your stack scanning and
remembered set scanning.  The current design does not need to do this
because the stack is so simple.

I am not sure this invariant holds.  Large objects tend to foul this
up.  You do not want to move these objects around and so the age
ordering invariant might not hold.  Of course, you can hedge against
this by having a special large object space where you stick the big
stuff.  This large object space should be part of the oldest
generation.

## Thoughts on adding a write barrier to static space
In an ideal world there would be little difference between dynamic
space and static space.  Static space would just be the part of the
heap that you inherit from the on-disk image.  Dynamic space would be
the part of the heap allocation when Lisp is started, out of fresh
anonymous pages.  Alas, we do not live in the ideal world, and so we
need to apply some of the storage performance optimizations for
dynamic space to static space.

Static space is a forward-allocated space.  Put another way, the
allocation mechanism is a simple bump pointer.  We don't have a notion
of allocating in pages like what gencgc.c does.  This means that the
gencgc mechanism for tracking pages is not so meaningful for covering
static space.

Instead, what we can do is break-up the page_table data structure into
the components we need for all pages versus the components needed for
dynamic space pages.  If we want to add a write barrier to static
space, we need the page_table::flags member.  So we'll want that.  The
other two words for bytes allocated and first object offset are
irrelevant.

Ray asks, is the first object offset irrelevant?  Is he right?  Maybe!
It all depends on how the static space is scanned.  This presents an
interesting challenge.  If we scan from bottom to top we can easily
parse the heap since objects are allocated densely.  If we want to
scan all objects on a page, we need a data structure for that, to say
how far back to look.  The LispM called this the "structure header
table" (I think).  The CLR calls this a "brick table".  Basically, you
look at another parallel array to the heap and it says how far back to
look to find the header for the object whose words are at the start of
the page.

Actually, I (Ray) thinks the this is the opposite of LispM.  We don’t
search backwards.  Instead, we just skip forward to the first object
offset and start scanning there.  But I’m not sure what happens when
we’re scanning forward and the object crosses the current page into
the next.  Do we just cross the boundary to scan?

What's the challenge?  Something has to create this dynamically.  So
maybe I (Carl) am wrong about the useless parts of the gencgc
page_table.  maybe we do want at least the first object offset since
that's relevant for finding out where to start scanning.  In that
case, we can probably reuse that structure as is and just ignore the
bytes allocated member.

Ray says that the bytes used might be needed to indicate where the end
of the page is.  There are separate free points for static space
inside the runtime. If we want to use this value (or at least track
it) it'll have to be kept in sync with the globals.

If the page table becomes sparse we can use a two scheme where we have
a bitmap that says whether this region of pages is allocated and scan
that first, using set bits as an index into the page_table, rather
than walking the page table on its own.  This trick was documented by
Lucid aeons ago.  (Their card marking marks twice.)

## Additional changes
### Maybe get rid of the read only scavenging stuff? 

(Seems like dead code) I (rtoy) have never set the bit, so scavenging
of the read-only space hasn’t been tested in decades, literally.

#### Status
See !58

### Maybe get rid of the write protect cleared stuff?

(Also seems like dead code) A quick grep of gencgc.c shows that
`PAGE_WRITE_PROTECT_CLEARED_MASK` is used in just one place (line 498)
where flags is updated with this bit set.  It doesn’t appear to be
read at any time.

#### Status
See !57
