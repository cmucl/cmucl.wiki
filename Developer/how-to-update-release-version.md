# How to Update CMUCL Release and Make a Release

To update the CMUCL release version, follow these simple steps.

1. Check out a new branch for the release:  `git checkout -b branch-21a` and switch to that branch.[^1]
1. Update [`BYTE-FASL-FILE-VERSION`](https://gitlab.common-lisp.net/cmucl/cmucl/blob/master/src/compiler/byte-comp.lisp#L41) to the desired version.
    * Recall that the version number is actually a set of hex digits.
    * The least significant digit must always be from the set of a-f.
1. Create a boot file in the appropriate directory.
   1. Copy, say, [`src/bootfiles/20e/boot-20f.lisp`](https://gitlab.common-lisp.net/cmucl/cmucl/blob/master/src/bootfiles/20e/boot-20f.lisp).
   2. Edit it and replace all occurences of 20f with 21a (or the appropriate version.)
1. Update `src/general-info/release-<n>.txt`

That should do it.  Now do a normal build using the -B option, specifying the boot file that was just created:
```
bin/build.sh -B boot-21a -U -C <options> -o lisp
```
The `-U` option will update the translations.  If files were updated, commit these changes.

When you're satisfied the everything builds ok, create an annotated tag for the release: `git tag -a 21a`.  Change repository to that tag and build again.  The resulting binary should announce itself with something like

>CMU Common Lisp 21a (21A Unicode),

Check that `*features*` contains `:CMU21A` and `:CMU21`.  Make sure there are no stray older versions like `:CMU20F` or anything like that.  If there are, they need to be removed.  (This shouldn't be necessary, but check anyway.)

If all of these things are true, you've successfully created a new
release. You may now want to update
https://gitlab.common-lisp.net/cmucl/cmucl/-/releases with the new
release information.  You can just copy the file `release-21a.md` to
this if you like.

Also update the [Wiki homepage](https://gitlab.common-lisp.net/cmucl/cmucl/-/wikis/home)
with the release information.  The website https://cmucl.org also needs
updating so update
the [master index at cmucl.common-lisp.dev](https://gitlab.common-lisp.net/cmucl/cmucl.common-lisp.dev/-/blob/master/cmucl-www/www/index.html)
with the release information.  This will get copied to https://cmucl.org
automatically after a short time.

Send an email to "cmucl-imp", "cmucl-announce", and "cmucl-help", if desired.

Finally, create a distribution of the results and upload them to `https://common-lisp.net/project/cmucl/downloads/release/<n>/`.

[^1]: We are inconsistent in naming the branch for a release.  We have
`branch-21e`, but also `21c-branch` and `21d-branch` and even
`RELEASE-20C-BRANCH`.  Let's be consistent from now on and use
`<n>-branch` for the branch for making release `<n>`.
