Since cmucl is a 32-bit application and most Linux distros are 64-bit,
you need to get 32-bit libraries and tools to be able to build cmucl.

Here is a brief set of instructions on how to get the tools needed to
build cmucl.

# Fedora
* C compiler and libraries
  * `sudo dnf install libgcc.i686 glibc-devel.i686`
  * `sudo dnf install clang` if you want to use clang
* X
  * `sudo dnf install libXt-devel` to get `libXt`, `libX11`, `libXau`,
    and others that are needed for X support
* Motif
  * `sudo dnf install motif-devel` for Motif. (Lots of other things
    get installed too.)

You will also need to get `make` and `cc` if you didn't install clang.  The easiest way to get these is to run them.  Fedora will ask if you want to install these.  Say "y" of course.

# OpenSUSE

Install the following:
* Git
  * `sudo zypper install git`
* C compiler and tools  
  * `sudo zypper install make`
  * `sudo zypper install gcc gcc-32bit`
* Motif and other X libraries  
  * `sudo zypper install motif motif-devel`

# Ubuntu
* C compiler and libraries
  * `sudo apt-get install gcc-multilib`
  * `sudo apt-get install clang` if you want to use clang
* X
  * `sudo apt-get install libxt-dev` to get `libXt`, `libX11`, `libXau`,
    and others that are needed for X support
* Motif
  * `sudo apt-get install libmotif-dev` for Motif. (Lots of other things
    get installed too.)


# MacOS

This is based on MacOS High Sierra (10.13.6), which is the OS used for
cmucl's CI.

* Install [Xcode 9.4.1](https://developer.apple.com/download/all/).
  (You need a free Apple ID to get this.)  You can also try [Xcode
  Releases]https://xcodereleases.com).  Search for "Xcode 9.4.1" to
  find the download link.
* Install the command line tools via `xcode-select --install`
* Install
  [XQuartz](https://www.xquartz.org/releases/XQuartz-2.8.1.html) for
  the libraries needed for X11.  This is needed to run CLM and
  Hemlock.  If you don't care for either, XQuartz is not necessary.

The version of git included with Xcode does not know how to handle the
SSL cert from common-lisp.net.  One possible solution is to install
MacPorts:

* Install [MacPorts for High Sierra](https://github.com/macports/macports-base/releases/download/v2.7.2/MacPorts-2.7.2-10.13-HighSierra.pkg)
* `sudo /opt/local/bin/port install git`
* `sudo /opt/local/bin/port install openmotif`

This version of git is able to do a git clone of cmucl.  Openmotif is
needed for CLM.

# Solaris/x86

It probably helps a lot to use [OpenCSW](opencsw.org) to get the
necessary tools.  Download the basic package from there.  I don't
remember what's included, but you need at least `git` and `gmake`.

Currently, we only have a config for building with Sun C so you need
to get [Oracle Solaris Studio
Compiler](https://www.oracle.com/tools/developerstudio/downloads/developer-studio-jsp.html).
Make sure `cc` can be found in your `$PATH`.

For motif, you need to do
```
sudo /usr/bin/pkg install pkg:/library/motif
```

# Testing the Build
To test that the libraries are working, try the following.
## Test CLM
To test CLM, try this:
```
$ lisp
* (require :clm)
* (break)
```
This should bring up the Motif debugger.  If you get an error like
```
Asynchronous NAME-ERROR in request 11 (last request was 12)  Code 45.0 [OpenFont]
```
it might be because you don't have the necessary fonts installed.

Try
```
sudo dnf install xorg-x11-fonts-75dpi
```

## Test Hemlock
To test hemlock, try:
```
$ lisp
* (require :hemlock)
* (ed "abc") ; "abc" is just some random file name
```
This should bring up the hemlock editor.  If it doesn't, you might be missing some fonts.

# VirtualBox
It's useful to be able to run some OSes in VirtualBox or other
virtualization software.  We include here some instructions on how to
do that.

See https://luppeng.wordpress.com/2023/10/08/setting-up-a-self-managed-gitlab-runner-with-the-virtualbox-executor/

## Linux
Nothing really special needed here.  Just get your favorite Linux
distribution and install it in VirtualBox.  A disk of 32GB is plenty
as is 1 CPU and 2 GiB of memory.

## Mac
This is a bit more complicated than Linux.  When creating a new VM, be
sure to specify 2 CPUs and 128 MB of video memory.  You need at least
32GB and probably more like 64GB of disk space to be able to install
Xcode and other tools.  4 GB of memory is enough, but 8 GB or more is
probably better since High Sierra recommends at least 8.

Get the [High Sierra
(10.13) ISO](https://archive.org/download/macos_iso/HighSierra_10.13.6.iso).

Finally consider using
[VBoxMacSetup](https://github.com/hkdb/VBoxMacSetup) to prepare MacOS
High Sierra with full screen resolution.  You may have to change the
`setup.sh` script to use "vboxmanage" instead of "VBoxManage" if
installing on Linux.  Run `./setup.sh ./setup.sh -v MacOS-10.13 -r
1920x1080`.  Replace "MacOS-10.13" with the name of the VM you
created.  Adjust the resolution if you want something besides "1920x1080".

Also, when installing to the new disk, you have to first use Disk
Utility to partition the VirtualBox disk before installing.  Otherwise,
the OS installer can't find any disks to install to.  There should be
an internal disk named something like "VBOX HARDDISK Media" that is
the approximately the size you used to create the VM.  Select "Erase"
and choose an appropriate "Name".  Leave everything else as is.
Erasing should be fast.  Exit Disk Utility.

Install macOS now selecting the disk that was erased above. It may take 20 minutes or more, depending on how fast the host machine is.

Once the OS is installed, install the necessary tools as above.

**Note**: When using VirtualBox 7.0.18, there is an issue with saving the
machine state on macos.  It just hangs.  To work around this,
configure the VM to use USB 3.0 instead of whatever is the default.
This is really important when using the VM as a gitlab runner because
it wants to make snapshots.  Without this USB 3 change, the VM just hangs
creating the snapshot so the runner gets stuck.

(More instructions needed.)
