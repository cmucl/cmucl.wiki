# Sparc64 Dev
Some random notes about sparc64-dev.

Because we have an existing, working sparc build, we can start the sparc64 code based off of the working sparc build.

##  Tags
This section contains a list of various tags on the sparc64-dev branch and what they mean.

1. sparc64-dev-checkpoint-0

    ```
    bin/build.sh -b sparc64 -C sparc64_sunc -o cmulisp
    ```
    builds a working lisp using only the files from the
    sparc64 directories.  (Verified by moving the sparc files
    out of the way.)
    
    The resulting lisp runs the full testsuite without errors.
    
    Snapshot 2016-12 was used to do the build on smalltalk.cs

1. sparc64-dev-checkpoint-1

    Cross-compile from x86 to sparc64 works
    
    Of course, this is still just a plain sparc 32 build, but
    this means things are in good shape for real sparc64 work.
    
    What happened: On darwin:
    ```
    bin/create-target.sh sparc64-xtarget sparc64_sunc
    bin/create-target.sh sparc64-xcross sparc64_sunc
    bin/cross-build-world.sh -c sparc64-xtarget/ sparc64-xcross/  src/tools/cross-scripts>/cross-x86-sparc64.lisp cmulisp
    ```
    Then
    ```
    tar cjf sparc64.tar.bz2 sparc64-xtarget
    ssh sparc64.tar.bz2 <smalltalk>
    ```
    On smalltalk:
    ```
    tar xjf sparc64.tar.bz2
    bin/rebuild-lisp.sh sparc64-xtarget
    bin/load-world -p sparc64-xtarget
    bin/build.sh -b sparc64 -C sparc64_sunc -o sparc64-xtarget/lisp/lisp
    ```
    When this is finished, do
    ```
    bin/make-dist.sh -I dist-sparc64 sparc64-4
    bin/run-tests.sh
    ```
    All tests passed.  At this point we're in good shape to begin developing sparc64.
1. sparc64-dev-checkpoint-2

    Like sparc64-dev-checkpoint-1, but we've set a new fasl file implementation value and changed the fasl file type to "sparc64f".  Cross-compile works and the resulting binary builds itself successfully and pass all tests.

1. sparc64-dev-checkpoint-3

    Updated `word-bits` to 64, adjusting new-genesis to byte-swap
    64-bit values if needed.

    Updated `Config.sparc64_sunc` to build a 64-bit binary, but using
    the abs32 memory model for now to keep things simple.

    Cross compile works but loading the world (on sparc)
    fails. (Issues with the core file)

1. sparc64-dev-checkpoint-4

    Updated `new-genesis.lisp` to create 64-bit core files.  This
    includes 64-bit values for space addresses and sizes and
    such. (Probably much more work needed.)

    The cross-compiled result will loads the core file and at least
    prints out the correct sizes of addresses of each space.  We get
    as far as running `call_into_lisp` to run the initial function,
    but we get a segfault because we haven't implemented the v9 stack
    bias. 

1. sparc64-dev-checkpoint-5

    Pad data blocks to dual-word (16-byte) boundaries.  The static symbols like `NIL`, `T`, etc. appear to have the correct addresses and contents, as determined by call C `print()` to print the contents of the object.

## Approach
The following steps indicate the general approach.  Since we've never actually done this completely, there will be some variations and corrections over time.

- [x] Copy over the sparc-specific stuff to sparc64 and do a sparc build using these.  This should result in a working sparc build.
- [x] Do a sparc64 cross-compile
  - [x] Verify tests pass.
  - [ ] Fix alien-callback issue wherein the cross-compiled build won't successfully build itself if alien-callback.lisp is compiled.
- [x] Change fasl name and version
  - [x] `sparc64f` instead of `sparc`
  - [x] Update fasl implementation number
- [ ] Update sparc64 code to 64-bit
  - [x] Change sparc64/parms.lisp to use 64-bit.
  - [x] Adjust `new-genesis.lisp` and `load_core_file` to dump 64-bit cores.
  - [ ] Update objdef.lisp to reflect the 64-bit properties.
  - [ ] Update insts to use the correct 64-bit versions
  - [ ] Update vops for 64-bit code.

