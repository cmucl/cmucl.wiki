# How to Make a Snapshot Release

Between formal releases, we often make snapshots of the current code for users to test.  They used to be done on a monthly basis, but are now more sporadically done.

[[_TOC_]]

## Preparations
Before creating a snapshot, make sure you can do a build from the latest source code and that all expected tests pass.

1. Choose a suitable directory to work in for the snapshot
2. Run `git clone git@common-lisp.net:cmucl/cmucl.git snapshot-yyyy-mm`, where "yyyy-mm" is the year and month of the snapshot.  All snapshots are named by the year and month.
3. `cd snapshot-yyyy-mm`
4. `./bin/build.sh -C "" -o lisp`
5. `./bin/make-dist.sh -SI dist build-4`, where "build" may depend on the OS, but is usually "linux" for Linux builds or "darwin" for MacOS builds.
6. `./bin/run-unit-tests.sh -l dist/bin/lisp`; no unexpected failures
7. `./bin/run-ansi-tests.sh -l dist/bin/lisp`; no unexpected failures

## Tag the snapshot
Now we're ready to tag the snapshot

1. `git tag -a snapshot-yyyy-mm` to create an annotated tag.
2. `git checkout snapshot-yyyy-mm`
4. `./bin/build.sh -C "" -o lisp`
5. `./bin/make-dist.sh -SI dist build-4`, where "build" may depend on the OS, but is usually "linux" for Linux builds or "darwin" for MacOS builds.
5.  Run `dist/bin/lisp -noinit` and verify the banner says
"snapshot-yyyy-mm (21E Unicode)".  The release version may not be
21E.  Make sure it doesn't say "dirty".
5.  `./bin/make-dist.sh -SI dist` build-4
6. `./bin/run-unit-tests.sh -l dist/bin/lisp`; no unexpected failures
7. `./bin/run-ansi-tests.sh -l dist/bin/lisp`; no unexpected failures

If all goes well, you can push the tag using
```
git push origin snapshot-yyyy-mmm
```

## Creating Tar files
Finally, you can create tar files to upload to the snapshot directory
on common-lisp.net.

Create the tarballs, including a tarball of the sources
```
bin/make-dist.sh -S build-4
```

### Signing the Tarballs
While not strictly necessary, all the releases have been signed.  You
can do this to sign them:
```
for f in cmucl-src-yyyy-mm.tar.bz2 cmucl-yyyy-mm-linux.*.bz2
do
  gpg -bsa $f
done
```

NOTE:  It is an historical artifact that these are signed using
rtoy's gpg key.  Ideally we should use a key owned by the cmucl
project.

## Uploading
Finally, you can upload these files to common-lisp.net

### Create the Download Directory
1. Login to common-lisp.net
2. Create the directory `/project/cmucl/downloads/snapshots/yyyy/mm/`
3. Logout

### Upload the Files
1. `scp src/general-info/release-21f.md cmucl-src-* cmucl-yyyy-mm*
<userid>@common-lisp.net:/project/cmucl/downloads/snapshots/yyyy/mm/`

In this example, we're upload the release notes release-21f.md
assuming that 21e has been released and we're working towards 21f.

## Final Steps
Once this has all been done, feel free to send an announcement to
cmucl-imp@cmucl.cons.org and cmucl-help@cmucl.cons.org or other
mailing lists.

Also, [make a
release](https://gitlab.common-lisp.net/cmucl/cmucl/-/releases/new).
You can copy the release notes here if you want. Look at previous
releases like [Snapshot
2023-04](https://gitlab.common-lisp.net/cmucl/cmucl/-/releases/snapshot-2023-04).
Be sure to add links to the binaries once they've been uploaded.

Uppdate the [wiki](home) with the information.  The
[website](cmucl.org) normally only has information about major version
releases, so snapshots don't need to noted there.

Finally, update [.gitlab-ci.yml](.gitlab-ci.yml) to use the snapshot for running the CI.

## Problems
Sometimes the snapshot doesn't go as planned.  If necessary, delete
the tag and start over.  The most common problem, however, is that
lisp announces itself as "snapshot-yyyy-mmm-dirty".  This is usually
caused by ".pot" files that didn't get updated.  If you're on Linux,
update the pot files and start again.  Delete the tag if necessary.

On MacOS and other OSes, there's a chance that the pot file was
changed.  See #223 for more info.  In this case, just revert the
changes and run
```
bin/load-world.sh dist
```
This should fix the dirty issue.   Then you can continue with the release.




