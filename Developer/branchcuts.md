# Branch Cuts
Branch cuts for the special functions are hard.  Here's how they're done in cmucl.

# Basics

In this we follow Kahan in how to interpret mathematical operations.

Let $`r`$ be a real number and $`z = x + iy`$ be a complex number whose real and imaginary parts are $`x`$ and $`y`$, respectively.  Then

```math
r + z = r + x + iy
```
and
```math
r - z = r - x - iy
```
Note that this is distinctly different from normal Common Lisp contagion rules which say $`r`$ is converted to the complex number $`r + 0i`$ before performing the operation.

Likewise, for multiplication we have
```math
rz = rx + iry
```
and
```math
iz = ix - y
```