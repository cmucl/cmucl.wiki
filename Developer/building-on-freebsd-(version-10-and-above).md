Prerequisites for Building CMUCL FreeBSD version
------------------------------------------------------

FreeBSD no longer uses GCC as the default compiler. Clang, the new compiler, will not build a working lisp loader binary. GCC must be used instead. I built with GCC 6 but literally any version should work.

Make sure that after you install your GCC you add a link

```
cd /usr/local/bin
ln -s gcc6 gcc
```

Make sure 32-bit compatibility libraries are installed (i.e. you have a /usr/lib32 directory). Make sure you've appropriately invoked ldconfig. This is necessary to RUN cmucl in FreeBSD's 64-bit environment at this point.

If you forgot to install the 32-bit libraries when you installed, you can do it manually.

```
cd /
tar xvf <path to lib32.tgz on the install media>
```

Or you can fetch lib32.tgz from the FreeBSD web site:

```
fetch ftp://ftp.freebsd.org/pub/FreeBSD/releases/amd64/10.1-RELEASE/lib32.txz
```

(modify the above to match your FreeBSD version.)

Then run
```
tar -xvpJf lib32.txz -C /
```

If you type

```
ldconfig -r | grep lib32
```

you should see the libraries in /usr/lib32. If not run

```
ldconfig -32 /usr/lib32/
```

Once you have taken these steps, the ordinary build instructions should work.