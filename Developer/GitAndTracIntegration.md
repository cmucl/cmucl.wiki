<!-- Name: GitAndTracIntegration -->
<!-- Version: 1 -->
<!-- Last-Modified: 2014/11/28 11:21:40 -->
<!-- Author: rtoy -->
## Git and Trac Integration

This page is obsolete since cmucl no longer uses trac.

Git is integrated with Trac so the commit messages can refer to
and even close Trac tickets.  If the commit messages contains text
like
```
    command #1
    command #1, #2
    command #1 & #2
    command #1 and #2
```
then the given command is applied to the specified tickets.
Instead of the short-hand syntax above, you can also use
```
    command ticket:1
    command ticket:1, ticket:2
    command ticket:1 & ticket:2
    command ticket:1 and ticket:2
```

The available commands (not case-sensitive) are:

   * close, closed, closes, fix, fixed, fixes

     The specified issue numbers are closed with the contents of this
     commit message being added to it.
   * references, refs, addresses, re, see

     The specified issue numbers are left in their current status, but
     the contents of this commit message are added to their notes.

You may also use WikiFormatting in the commit message.  When trac
updates the ticket with the commit message, it will be nicely
formatted.
