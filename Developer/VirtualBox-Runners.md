# Continuous Integration with VirtualBox

For continuous integration, cmucl use self-hosted VirtualBox runners.  It's quite a bit of work to setup so we summarize the steps here.

## Setup VMs
See [BuildTools VirtualBox](Developer/BuildTools#virtualbox) for how to set up the Linux and MacOS VM using VirtualBox.  Make sure these VMs can clone the cmucl repo and also have all the tools to build cmucl.

## Set Up Runners
These are fairly difficult to do but [Setting Up a Self Managed GitLab Runner](https://luppeng.wordpress.com/2023/10/08/setting-up-a-self-managed-gitlab-runner-with-the-virtualbox-executor/) is extremely helpful.

Follow steps 1-3.  For step 3, we are currently using user-mode which means we need to start the runner manually as needed.

In step 4, since we're running both the Linux and MacOS runners from the same account, choose different ports on each runner.

In step 5, the command there didn't work.  I used `ssh-keyscan -p 2222 -H 127.0.0.1`.  This should produce some output.  Once you have this, you can append the results of `ssh-keyscan` to `.ssh/known_hosts`. Do the same for the port that is being used for the MacOS VM.

Also, at this step, it's useful to enable a bridged network interface so you can ssh in directly to the VM.  This is useful for setting up or installing things as needed in the VM.  Along these lines, enable ssh on the VM too.  Create an ssh key on the host.  Add this key to the the file `.ssh/authorized_keys` on the VM.  Then you don't need to have the password when logging in to the machine.  I think this is important for the runner because it needs to ssh in from the host to run things.

Follow steps 6.

For steps 7 and 8, you need to do this for both the Linux and MacOS runners.  Both configs will be safely appended to `config.toml`.

At this point, I stopped following the directions.  Instead, to start the runners, just run `gitlab-runner run` from a terminal.  You need to keep this running and therefore can't log out.  It's also useful for debugging since log messages are printed as things are requested.



## Testing
You should be able to see the runners at
https://gitlab.common-lisp.net/cmucl/cmucl/-/settings/ci_cd.  Of
course, for this to work, you have to set up `.gitlab-ci.yml`
appropriately to select the runner to use.  Currently, the runners are
named "Linux Virtualbox 2" and "MacOS VirtualBox".

Once `.gitlab-ci.yml` is set up, start a pipeline.  The pipeline
should run for both runners.

## Notes
If you need to install some new software or something, do that in the
base VMs.  Then delete the runner VMs.  Gitlab will regenerate these
as needed, cloning the base VM so that the clone will have the
installed software.

It might also be helpful to make sure `~/.gitlab-runner/config.toml`
has something like:
```
  [runners.ssh]
    user = "<user>"
    identity_file = "<gitlab/.ssh/id_rsa"
    disable_strict_host_key_checking = true
```
I think this requires that you can `ssh` into machine without entering
a password, so be sure to update `.ssh/authorized_keys` on the runner
machine to have an appropriate key.

### Issues with Runners
These are some of the generic issues encountered with the runners.

* #370: VirtualBox runners fail to start with VMX root error

The sections below lists some specific issues with the different
runners.

#### Linux
For some reason, CI won't work on Linux if the base VM is not running.
Thus, start the base VM and leave it running.  The error is that ssh
couldn't connect.  I don't know why; MacOS doesn't need this; it works
fine without starting either the base VM or the runner VM.


#### MacOS
The version of git that comes with the OS is too old to recognize the
SSL cert.  A newer version of git is needed; I used [MacPorts](https://www.macports.org) since
that's what was used on the original CI machine.  

Not sure what happened but recent pipelines were failing because the
git repo couldn't be updated because of the SSL cert issue.  The fix
was to modify /etc/profile and add /opt/local/bin to `PATH` so that
the newer version of git can always be found.  This seems to fix the
issue.

See also #358 where the certificate was not yet valid.  This was due
to a stale certificate as mentioned in #358.

## Tweaking Config
If the runners are on separate machines, then the runners will run in
parallel, obviously.  But if the runners are all on one machine (as it
is currently), only one job will run at a time by default.  That is,
there is no parallelism.

To change this, see 
[concurrent setting](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-global-section).
The default is one; if the host is powerful enough with enough CPUs
and memory, increase the value
of `concurrent` to something larger.  Two is probably a decent value
for machine with 6 cores (12 threads) and 32 GB.

However, it appears that currently the CI pipeline is **not** quite set up to run in parallel.  Not sure why, but it needs some investigation before being able to do parallel builds.
