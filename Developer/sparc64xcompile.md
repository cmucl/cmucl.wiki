Some random notes on how to do a linux to sparc64 cross-compile.  WIP.

Build the sparc64 port on linux:
```
git co sparc64-dev
bin/cross-build-world.sh -c sparc64-xtarget sparc64-xcross/ src/tools/cross-scripts/cross-x86-sparc64.lisp cmulisp
tar cjf sparc64-xtarget.tar.bz2 sparc64-xtarget
```
Now copy the tarball to sparc host machine.  Then do:
```
git co sparc64-dev
tar xjf sparc64-xtarget.tar.bz2
bin/rebuild-lisp.sh sparc64-xtarget
bin/load-world.sh -p sparc64-xtarget
```
If you know `internals.h` and friends haven't changed, you replace the `rebuild-lisp.sh` step with just `(cd sparc64-xtarget/lisp; make)`