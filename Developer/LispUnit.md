<!-- Name: LispUnit -->
<!-- Version: 2 -->
<!-- Last-Modified: 2014/03/15 10:30:44 -->
<!-- Author: rtoy -->
# Lisp Unit

`CMUCL` uses [Lisp-Unit](https://github.com/OdonataResearchLLC/lisp-unit/wiki) for unit testing.

The test suite should be run before a snapshot is tagged and released.

To run the test suite:

1. You must be in the top of the CMUCL source tree.
1. If you have not installed CMUCL, you must install it.
   1. Run `bin/make-dist.sh -I <dir>` to install it. 
1. For the full test suite, run `<lisp> -noinit -load tests/run-tests.lisp -eval '(cmucl-test-runner:run-all-tests)'`
1. To run just one of the test suites:
   1. Run lisp
   1. `(require :lisp-unit)`
   1. Load one of the files in `tests`
   1. Run the test using `(lisp-unit:run-tests :all <package>)`.
      1. The package name is the file name appended with "-TESTS". For example, the trig tests are in `trig.lisp` and the package name is "TRIG-TESTS". 


