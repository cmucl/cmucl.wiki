# How to Define a New Instruction

## Testing

Instead of doing a full build, you can incrementally test your instruction by compiling and loading "insts.lisp". To have the new instructions take affect, do:

```plaintext
(setf (disassem::params-inst-space (c::backend-disassem-params c::*native-backend*)) nil)
```