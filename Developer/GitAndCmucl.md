<!-- Name: GitAndCmucl -->
<!-- Version: 11 -->
<!-- Last-Modified: 2014/11/28 11:22:08 -->
<!-- Author: rtoy -->
# Git and CMUCL


<!-- [[PageOutline]] -->
## Git
The CMUCL CVS repository was converted to git on 2011-09-20 and is now
available.  You can browse the repository using the
[Browse Source link](http://trac.common-lisp.net/cmucl/browser)
above, or you can visit
[CMUCL gitweb](http://common-lisp.net/gitweb/?p=cmucl/cmucl.git;a=summary;js=1)
for a different view.  This latter page also includes links necessary for
cloning the repository.  (For committers, be sure to put your
userid in the ssh link.)

In short, to get a copy do
```
   git clone git://common-lisp.net/projects/cmucl/cmucl.git
```
or, if the git method does not work, use the http transport
```
   git clone http://common-lisp.net/r/projects/cmucl/cmucl.git
```

Developers should use
```
   git clone ssh://user@common-lisp.net/var/git/projects/cmucl/cmucl.git
```
where `user` should be replaced with your common-lisp user name.

The CVS repository will still be available, but will not allow
checkins anymore.

## Mirrors

There is also a [git mirror at Github](https://github.com/rtoy/cmucl).
It may be slightly out-of-date compared to the official
repository, but the intent is that it should never be more than a few
days behind, and hopefully no more than a few hours.

## Tips

### CVS
The directory structure for CMUCL was changed on 2011/11/04 in
changeset [a9961276fedcff5f7e94a07d334546c819dd420a].  This makes
`git log` not produce the history of the changes before that date.
However, you can still get the previous history using 
`git log --follow`.

### Git
To keep the master branch in a cleaner state without merge
commits littering the history, we suggest using one of the following:

 1. `git pull --rebase`
 1. Edit `.git/config` so that it contains something like
```
[branch "master"]
  remote = origin
  merge = refs/heads/master
  rebase = true
```
 1. Add the following to `~/.gitconfig` 
```
[branch]
  autosetuprebase = true
```

You can read more about it at
[http://viget.com/extend/only-you-can-prevent-git-merge-commits](http://viget.com/extend/only-you-can-prevent-git-merge-commits),
[http://mislav.uniqpath.com/2013/02/merge-vs-rebase/](http://mislav.uniqpath.com/2013/02/merge-vs-rebase/),
[http://stackoverflow.com/questions/8509396/git-pull-results-in-extraneous-merge-branch-messages-in-commit-log](http://stackoverflow.com/questions/8509396/git-pull-results-in-extraneous-merge-branch-messages-in-commit-log).

