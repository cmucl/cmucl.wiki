<!-- Name: GettingCmucl -->
<!-- Version: 6 -->
<!-- Last-Modified: 2014/10/12 09:24:48 -->
<!-- Author: rtoy -->
# How to Get CMUCL

## Sources
For the latest sources, look at [GitAndCmucl](https://gitlab.common-lisp.net/cmucl/cmucl/wikis/GitAndCmucl).

## Binaries
The latest formal release of CMUCL (binaries and source) is available
from the various download sites, in the release directory. For each
supported platform there are two bzipped tar archives, one containing
the base system, and the other (with "extra" in the name) containing
additional files supporting CLX, Hemlock, Gray streams and so on. In
addition, there is the source archive containing the sources used to
compile the binaries.

The download sites are:

 * <http://common-lisp.net/project/cmucl/downloads/>: The primary
   CMUCL machine.
 * <http://www.cmucl.org/downloads/>: The [main CMUCL
   website](http://www.cmucl.org)  has a
   mirror of the common-lisp.net downloads directory.
 * <http://pmsf.eu/pub/cmucl/>: A mirror of the downloads directory,
   located in Germany with good bandhwidth across Europe and the USA.
 * <http://www.rgrjr.com/cmucl/downloads/>: A mirror of the downloads directory.

Monthly binaries are also available from the download sites. These are
binaries that are built at the beginning of each month (roughly) and
are based on the code base at that time. These may differ
significantly from the release, because they may have bug fixes or new
features added. Although we try to be sure that they work and are
capable of compiling themselves, they are only lightly tested. Use
with caution.

Binaries built from source more recent than the latest release are
available from the download mirrors in the binaries directory. These
binaries contain bugfixes and new features that are not in the release
binaries, but have had less testing.

Binaries providing experimental features (such as long floats), or
built with non-standard features or subsystems, are available in the
experimental directory. These haven't had much testing.

