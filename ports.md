# Ports: software which runs in CMUCL

This page contains an (incomplete) list of freely redistributable software which is known to run in CMUCL (to know what platforms CMUCL runs on, see the platforms page).

[[_TOC_]]
## Networking

*    [Araneida](https://www.cliki.net/araneida">Araneida) is a **small extensible web server** written in Common Lisp with CMUCL extensions, under a BSD-sans-advertising licence. It hooks into the CMUCL event loop rather than using multithreading, and is designed to run behind a caching proxy such as [Apache's mod_proxy](http://httpd.apache.org/docs/current/mod/mod_proxy.html).
*    [CL-HTTPD](http://www.ai.mit.edu/projects/iiip/doc/cl-http/cl-http.htm) is an advanced web server implemented in Common Lisp.
*    [CLORB](http://clorb.sourceforge.net/) is a [CORBA](https://www.corba.org/) ORB for Common Lisp. It is still under development, but the client-side functionality works with CMUCL (server-side is currently limited to single-threaded polling).
*    The [CLiki](https://www.cliki.net) is a [wiki-like](http://meatballwiki.org/wiki/MeatballWiki) system allowing collaborative web authoring, developed in CL.

## Graphics

*    *CLX* is an implementation of the client side of the X11 protocol in Common Lisp (ie it plays the role of libX11 for C programs). The CMUCL version incorporates patches for X authorization which are not included in the official release.
*    [McCLIM](http://common-lisp.net/project/mcclim/) is a rapidly improving free implementation of the CLIM specification. This library allows rapid development of powerful inspector-type user interfaces.
*    [Garnet](http://garnetlisp.sourceforge.net/) is a **sophisticated widget set** developed at the User Interface Software Group at Carnegie Mellon University. Garnet aims to facilitate the construction of interactive graphical user interfaces. It includes an implementation of a prototype-instance object model, a portable graphics layer for X11 and MacOS, automatic constraint maintenance, and interactive design tools. [Plot-2d](http://www.eis.mdx.ac.uk/staffpages/rvb/software/plot-2d/) is a plotting gadget for Garnet, that makes it easy to visualize two-dimensional data.
*    **CLM** is a Motif® subsystem for CMUCL. It can be compiled with the freely available [LessTif](http://lesstif.sourceforge.net/) implementation. CLM is used in the graphical interface to the CMUCL debugger and inspector.
*    There are two CMUCL bindings to the [GTk+ widget set](https://www.gtk.org/). [Espen Johnsen's clg](https://sourceforge.net/projects/clg) binds directly to the GTk event loop via the CMUCL FFI (GNU LGPL). [Gilbert Baumann's (older) binding](https://www.cliki.net/cl-gtk) uses a client-server model to avoid problems with mixing two event loops.
*    [GLOS](https://sourceforge.net/projects/glos/) is an OpenGL subset developed in Common Lisp. It features Gourad shading, Phong lighting, Zbuffer/clipping, near and far.
*    [CLUE](http://www-cgi.cs.cmu.edu/afs/cs/project/ai-repository/ai/lang/lisp/gui/clue/) (Common Lisp User-Interface Environment) is an extension of CLX which provides a simple, object-based toolkit (similar to Xt), using CLOS. It provides simple object-oriented classes and stream I/O facilites. CLIO is a set of widgets built on top of CLUE, and Pictures is a CLOS-based interface to CLIO.
*    [PGPLOT-CL](http://www.geocities.ws/pgplot_cl/) is an CMUCL interface to the PGPLOT 2-d scientific plotting library.
*    The [PSGrapher](https://www.cliki.net/psgraph) renders a directed acyclic graph to Postscript. It consists of a set of Lisp routines which uses the same algorithm as the ISI Grapher. The fonts and box drawing style are user-definable.

## Mathematics

*    The [series package](https://github.com/rtoy/cl-series) (described in Appendix A of [Common Lisp: the Language, 2nd edition](http://www.cs.cmu.edu/afs/cs.cmu.edu/project/ai-repository/ai/html/cltl/cltl2.html)) is a set of macros which allow the programmer to program in a functional style (using compositions of functions operating on series or vectors or streams of data elements), without incurring the efficiency costs typically associated with such a programming style. The macros generate iterative loops.
*    [f2cl](https://gitlab.common-lisp.net/f2cl/f2cl) is a Fortran to Common Lisp compiler that translates Fortran 77 to fairly readable Common Lisp.
*    [MatLisp](http://matlisp.sourceforge.net/) is a matrix package for Common Lisp. It consists of CLOS bindings to the BLAS and LAPACK linear algebra packages.
*    [Maxima](http://maxima.sourceforge.net/) is a computer algebra system implemented in Common Lisp. It is a derivative of the Macsyma system, which is released under the GNU GPL. Maxima is primarily intended to be used with the GCL Common Lisp implementation, but recent releases also work with CMUCL.
*    [ACL2](http://www.cs.utexas.edu/users/moore/acl2/) is a semi-automatic theorem prover embedded in an applicative dialect of Common Lisp (it is the successor of Nqthm). The system can also be used as a specification language and a computation logic.

## Misc

*    [CL-XML](http://www.cl-xml.org/) implements data stream parsing and serialization for the XML, XML Query and XPath standards. The XML processor includes a conformant, validating, namespace-aware model-based parser. It supports, in particular, namespace-aware DTD-based validation. The XPATH module comprises LISP bindings for the XML Path library, an S-expression-based namespace-aware path model, and a macro-based path model compiler which implements an XPATH-algebra. The XQUERY module comprises LISP bindings for the XML Query library, an S-expression-based query model which incorporates the XPATH facilities, and a macro-based query compiler. The base CLOS model implements the XML Query Data Model in a class hierarchy which presents an Infoset compatible programming interface. CL-XML runs with CVS versions of CMUCL as of around 2003-08.
*    [Common Lisp Music](https://ccrma.stanford.edu/software/clm/) is a music synthesis and signal processing package in the Music V family.
*    [CLOCC](http://clocc.sourceforge.net/) includes 1mk-defsystem1, an implementation of the Common Lisp `DEFSYSTEM` utility (which occupies a similar position to make in C/Unix projects).
*    [LISA](http://lisa.sourceforge.net/) is a production-rule system for building intelligent software agent systems in Common Lisp. It is influenced by the Java Expert System Shell.
*    [OPS5](https://gitlab.common-lisp.net/cmucl/cmucl/tree/master/src/contrib/ops) is a programming language for production systems (using goal-based techniques from artificial intelligence). In the public domain.
*    [feebs](https://gitlab.common-lisp.net/cmucl/cmucl/tree/master/src/contrib/games/feebs) is a simulation game somewhat similar to CoreWars. Players supply programs which control their creature as they move around the maze trying to zap one another.
*    ~~[Surf-Hippo](http://surf-hippo.neurophysics.eu/Surf-Hippo.README.html) is a simulation package which allows interactive graphical exploration of models of biological neurons and networks. Surf-Hippo runs in CMUCL with Garnet.~~

If you successfully use a package in CMUCL, or produce patches adding CMUCL support, please let us know by sending an email to the webmasters (see address in footer).
