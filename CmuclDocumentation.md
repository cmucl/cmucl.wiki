<!-- Name: CmuclDocumentation -->
<!-- Version: 4 -->
<!-- Last-Modified: 2014/09/04 20:45:16 -->
<!-- Author: rtoy -->
# CMUCL Documentation

The main documentation reference for Common Lisp is, of course, the 
[Common Lisp HyperSpec](http://www.lispworks.com/documentation/HyperSpec/Front/index.htm).
For CMUCL, we have

 * The *CMUCL User's Manual*, a detailed document containing
   information on the Python compiler and CMUCL extensions, in several
   formats:
   * [HTML on one page](https://cmucl.org/docs/cmu-user/cmu-user.html)
   * [HTML on multiple pages](https://cmucl.org/docs/cmu-user/html/index.html)
   * [PDF (US
     letter-size)](https://cmucl.org/docs/cmu-user/cmu-user.pdf) with
     embedded hyperlinks
   * [Tarball of the HTML version on multiple
     pages](https://cmucl.org/docs/cmu-user/cmu-user-html.tar.bz2)
   * [Tarball of the info files](https://cmucl.org/docs/cmu-user/cmu-user-info.tar.bz2) for local use with Emacs
 * The *CLX* manual for 
   [online browsing](https://sharplispers.github.io/clx/) or
   in [gzipped tarred Postscript](https://cmucl.org/downloads/doc/clx-manual.tar.gz).
 * The *CMUCL Motif Toolkit*:
   * The User Guide:
     * in [HTML on one
       page](https://cmucl.org/docs/interface/toolkit/toolkit.html)
     * in [HTML on multiple
       pages](https://cmucl.org/docs/interface/toolkit/html/index.html)
     * as [PDF](https://cmucl.org/docs/interface/toolkit/toolkit.pdf)
   * Implementation guide:
     * in [HTML on one
       page](https://cmucl.org/docs/interface/internals/internals.html)
     * in [HTML on multiple
       pages](https://cmucl.org/docs/interface/internals/html/index.html)
     * as [PDF](https://cmucl.org/docs/interface/internals/internals.pdf)
 * The design document describing the *internals of CMUCL*. This is
   very incomplete:
     * in [HTML on one page](https://cmucl.org/docs/internals/design.html)
     * in [HTML on multiple pages](https://cmucl.org/docs/internals/html/index.html)
     * as [PDF](https://cmucl.org/docs/internals/design.pdf) (US
       letter size)
     * the [original PDF doc](https://cmucl.org/downloads/doc/CMUCL-design.pdf) for historical reference
 * [Internal design of CMU common Lisp on the IBM RT PC](http://repository.cmu.edu/cgi/viewcontent.cgi?article=2652&context=compsci), a nice
   introduction to the early design of CMUCL for the RT PC.
 * *Hemlock* is the integrated editor which comes with CMUCL.
   * The Hemlock User's Manual:
       * [HTML version all on one page](https://cmucl.org/docs/hem/user/hemlock-user.html) for online browsing
       * [HTML version all on multiple page](https://cmucl.org/docs/hem/user/html/index.html) for online browsing
       * [PDF (US letter size)](https://cmucl.org/docs/hem/user/hemlock-user.pdf) with embedded hyperlinks
       * [Tarball of the info files](https://cmucl.org/docs/hem/user/hemlock-user-info.tar.bz2) for local use with emacs
   * The Hemlock Command Implementor's Manual:
       * [HTML version all on one page](https://cmucl.org/docs/hem/cim/cim.html)
       * [HTML version on multiple pages](https://cmucl.org/docs/hem/cim/html/index.html)
       * [PDF (US Letter size)](https://cmucl.org/docs/hem/cim/cim.pdf)
       * [Tarball of the info files](https://cmucl.org/docs/hem/user/cim-info.tar.bz2) for local use with emacs
   * The original Scribe versions (of historical interest):
       * [Hemlock User's Manual (Postscript)](https://cmucl.org/doc/encycmuclopedia/devenv/user.ps)
       * [Hemlock Command Implementor's Manual (Postscript)](https://cmucl.org/doc/encycmuclopedia/devenv/cim.ps)


For some background on the CMUCL Project, see the [History of the CMUCL Project](HistoryOfTheCMUCLProject).
